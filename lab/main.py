# -+- coding: utf-8 -+-
#
# Created by philiam on 2015/11/23.
#
from __future__ import unicode_literals
from zbase.tools import *
import time
import json
import urllib

__author__ = 'philiam'


def test_online():
    url = 'http://config.invt.com:9600/api'
    data = {
        'u': 'gards_query/terminal/863092017073456/online',
        't': 'GET'
    }
    a = get_http_string(url, urllib.urlencode(data))
    jr = json.loads(a)

    print(jr['msg'])


def test():
    patterns = [
        # ('控制文字', '发往下位机的指令', '发完 回复给用户微信的信息'),
        (['真人语音模式'], '{"Act":"UpAppProfile", "AUDIO": "WAV"}', '指令已发送',False),
        (['新手模式'], '{"Act":"UpAppProfile", "MODE": "NEWBIE"}', '指令已发送'),
        (['专家模式'], '{"Act":"UpAppProfile", "MODE": "EXPERT"}', '指令已发送'),
        (['人工智能语音模式'], '{"Act":"UpAppProfile", "AUDIO": "TTS"}', '指令已发送'),
        (['英文模式'], '{"Act":"UpAppProfile", "LANG": "USA"}', '指令已发送'),
        (['中文模式'], '{"Act":"UpAppProfile", "LANG": "CHN", "DIALECT": "0"}', '指令已发送'),
        (['淮北话模式', '淮普模式'], '{"Act":"UpAppProfile", "LANG": "CHN", "DIALECT": "0561"}', '指令已发送'),
        (['常德话模式'], '{"Act":"UpAppProfile", "LANG": "CHN", "DIALECT": "0736"}', '指令已发送'),
        (['休眠周期20'], '{"Act":"SleepPeriod", "Period":"20"}', '指令已发送'),
        (['GS灵敏度高'], '{"Act":"GSAccuracy", "Accuracy":"5"}', '指令已发送'),
        (['禁用休眠'], '{"Act":"SleepDisable"}', '指令已发送'),
        (['启用休眠'], '{"Act":"SleepEnable"}', '指令已发送'),
        (['节能保护模式'], '{"Act":"PSaving"}', '指令已发送'),
        (['上位机复位'], '{"Act":"UpAppReset"}', '指令已发送'),
        (['后视镜关机'], '{"Act":"UpAppReset","POFF":"1"}', '指令已发送'),
        (['后视镜开机'], '{"Act":"UpAppReset", "PON":"1"}', '指令已发送'),
        (['切换道道通导航'], '{"Act":"UpAppProfile", "NAVIHOME":"RtNavi","NAVINAME":"RtNavi.exe","NAVITITLE":"RtNavi"}', '指令已发送'),
        (['测试发送导航'], '{"Act":"Navi","Lat":"131.000000","Lon":"121.000000","Coord":"WGS84","XCode":"","Name":"shanghai"}', '指令已发送'),
    ]
    text = '淮北话模式'
    for pattern in patterns:
        if text in pattern[0]:
            if pattern[3]:
                print(pattern[1])
                break


def urldecode(query):
    d = {}
    a = query.split('&')
    for s in a:
        if s.find('='):
            k,v = map(urllib.unquote, s.split('='))
            try:
                d[k].append(v)
            except KeyError:
                d[k] = [v]

    return d


def test2():
    ax = '__p=B02F38MTIF,22.661612,114.061771,7%E5%A4%A9%E9%85%92%E5%BA%97(%E6%B7%B1%E5%9C%B3%E5%9D%82%E7%94%B0%E5%8D%8E%E4%B8%BA%E5%9F%BA%E5%9C%B0%E5%BA%97),%E5%9D%82%E7%94%B0%E8%A1%97%E9%81%93%E9%9B%AA%E5%B2%97%E8%B7%AF2227%E5%8F%B7&callapp=0&poiid=B02F38MTIF'

    a = urldecode(str(ax))

    al = a['__p'][0].split(',')
    ai = al[3] + al[4]
    print str(ai.encode('ISO-8859-1'))


def test3():
    a = "123456df(bde)"
    print a.find('(')
    print a[:a.find('(')]
    a = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx4806b076e959f03f&redirect_uri=http%3a%2f%2fgards.covixon.com%2fapi%2foauth2%2f" + '1111' + "%2f&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect"
    print a

test3()
