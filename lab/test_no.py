# coding: utf-8
# author: ZhangTao
# Date  : 2016/5/6
# from __future__ import unicode_literals
from zbase.tools import *
import json
def get_test_token():
    token = "bWBQ7MoBYccOqTbpPNuPSGDziJW-OdyY8TVY6Yh5k_VLMrUeMCrYB8FVAwhq91WrrxA3Lh8AB-_MDMziFzPAGZs2aJdoykP3MJw5k9YLZkqqrAtHClKaLjX0lt3UwlQnNYBfCIAUFI"
    return token

def create_menu():

    url = 'https://api.weixin.qq.com/cgi-bin/menu/create?access_token=%s' % get_test_token()
    data = {
     "button":[
      {
           "name":"车辆",
           "sub_button":[
           {
               "type":"view",
               "name":"关联车辆",
               "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx4806b076e959f03f&redirect_uri=http%3a%2f%2fgards.covixon.com%2fapi%2foauth2%2f11%2f&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect"
            },
            {
               "type":"view",
               "name":"车辆位置",
               "url": "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx4806b076e959f03f&redirect_uri=http%3a%2f%2fgards.covixon.com%2fapi%2foauth2%2f12%2f&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect"
            },
            {
               "type": "view",
               "name": "车辆状态",
               "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx4806b076e959f03f&redirect_uri=http%3a%2f%2fgards.covixon.com%2fapi%2foauth2%2f13%2f&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect"
            },
            {
               "type": "click",
               "name": "最新故障",
               "key": "14"
            }
           ]
        },
        {
          "type":"view",
          "name":"车主",
          "url":"http://www.covixon.com/"
        },
        {
          "type":"view",
          "name":"服务",
          "url":"http://www.covixon.com/"
        },
     ]
    }
    # rs = get_http_string(url, json.dumps(data, ensure_ascii=False))
    r = urllib2.urlopen(url, json.dumps(data, ensure_ascii=False))

    log.info(r.read())


create_menu()