# coding: utf-8
__author__ = 'philiam'

import urllib2
import json
import time
# import logging
from wei.settings import *
from zbase.tools import *


def get_access_token():
    wxtoken = 'wxtoken.json'
    tj = open(wxtoken, 'r')
    if tj is not None:
        content = tj.read(10000)
        jc = json.loads(content)
        s_t = int(time.time())
        log.info('检查access_token是否过期')
        if s_t - jc['created'] < 5000:
            log.info('未过期直接返回')
            log.debug(jc)
            tj.close()
            return jc['access_token']
    tj.close()
    tj = open(wxtoken, 'w')
    AppId = 'wxae34c9c90bd90a6a'
    AppSecret = '40916acc5119249b406f2280a43d0954'
    log.info('已过期重新获取')
    url = 'https://api.weixin.qq.com/cgi-bin/token?' \
          'grant_type=client_credential' \
          '&appid=%s&secret=%s' % (AppId, AppSecret)
    # f = urllib.urlopen(url, json.dumps(post_data, ensure_ascii=False))
    f = urllib2.urlopen(url)
    r = f.read()
    log.debug('接口返回结果：%s' % r)
    jr = json.loads(r)
    s_now = int(time.time())
    jr['created'] = s_now
    tj.write(json.dumps(jr, ensure_ascii=False))
    tj.close()
    return jr['access_token']


# get_access_token()


def getQRCode(str):
    token = get_access_token()
    url = ''

    post_data = {
        # "expire_seconds": 1800,
        # "action_name": "QR_SCENE",
        "action_name": "QR_LIMIT_STR_SCENE",
        "action_info": {"scene": {"scene_str": str}}
    }

    url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=%s" % token
    log.info("发起微信二维码生成请求")
    log.debug(post_data)

    f = urllib2.urlopen(url, json.dumps(post_data, ensure_ascii=False))
    r = f.read()
    log.info("收到返回数据")
    log.debug(r)
    jr = json.loads(r)
    pic_url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=%s" % jr['ticket']
    log.info('二维码地址：%s' % pic_url)
    return pic_url
