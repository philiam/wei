# -+- coding: utf-8 -+-
#
# Created by philiam on 2015/11/17.
#
from __future__ import unicode_literals
import re
import requests
__author__ = 'philiam'

def testc():
    print("C")
    return 1

def test_reg():
    import re

    line = "广东省深圳市宝安区广东省深圳市宝安区龙华新区清湖工业园清祥路1号宝能科技园9栋A座10"
    print line.find('(')
    line = line[:]
    rules = [
        r'(.*?)省(.*)市(.*?)[区县](.*)',
        r'(.*)市(.*?)[区县](.*)',
    ]
    for regex in rules:
        matchObj = re.search(regex, line, re.M|re.I)
        if matchObj:
            print matchObj.lastindex
            print "matchObj.group() : ", matchObj.group()
            print "matchObj.group(1) : ", matchObj.group(1)
            print "matchObj.group(2) : ", matchObj.group(2)
            print matchObj.group(3)
            print matchObj.group(matchObj.lastindex)

        else:
           print "No match!!"
    print line
# test_reg()
# testc()


def get_navigate_dst(text):
    # 用正则匹配的方式进行简单语义解析
    # 比如:
    #  我要到世界之窗
    #  我要去世界之窗
    #  到世界之窗去
    #  导航到世界之窗

    rules = [
        r'(.*)到(.*)去',
        r'(.*)去(.*)',
        r'(.*)到(.*)',

        # 模式语音识别,导航到xxx 可能会被识别成 导航的xxx
        r'(.*)的(.*)去',
        r'(.*)的(.*)',

    ]
    for regex in rules:
        matchObj = re.search(regex, text, re.M | re.I)

        if matchObj:
            for i in range(matchObj.lastindex):
                print(matchObj.group(i))
            # print "matchObj.group() : ", matchObj.group()
            # print "matchObj.group(1) : ", matchObj.group(1)
            # print "matchObj.group(2) : ", matchObj.group(2)
            print matchObj.group(matchObj.lastindex)
            return matchObj.group(matchObj.lastindex)
        else:
            print("No match!![%s]" % text)
    return ''


def get_location():
    dst = get_navigate_dst('导航到地天泰电子有限公司')
    if dst:
        url = 'http://api.map.baidu.com/place/v2/search'
        data = {
            'output': 'json',
            'q': dst,
            'region': '深圳',
            'ret_coordtype': 'gcj02ll',
            'ak': 'xabAK2yF5G57GVOykz0BQXLy'
        }
        rs = requests.get(url, params=data)
        print rs.text


def get_address(lat=39.983424, lng=116.322987):
    url = 'http://api.map.baidu.com/geocoder/v2/'
    data = {
        'output': 'json',
        'location': '39.983424,116.322987',
        'coordtype': 'gcj02ll',
        'ak': 'xabAK2yF5G57GVOykz0BQXLy'
    }
    rs = requests.get(url, params=data)
    print rs.text

get_location()