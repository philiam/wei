# coding:utf-8

import requests
import hashlib
import datetime
import time

base = datetime.datetime(1900, 1, 1)
ts = datetime.datetime.now() - base


def quectel():
    url = 'http://139.196.41.136:8098/openapi/router'
    data = {
        'appkey': '5d428b3263',
        't': int(time.time()),
        'method': 'fc.function.sms.send',
        'msisdns': '1064868406171',
        'content': '*awaken*;'
    }

    sorted_data = sorted(data.items(), key=lambda d: d[0])
    print(sorted_data)
    app_secret = 'acdef0dfq'
    s = app_secret
    for k in sorted_data:
        s += '{}{}'.format(k[0], k[1])

    s += app_secret
    sign = hashlib.sha1(s).hexdigest()

    print(s)
    data['sign'] = sign
    # import json
    # print (json.dumps(data,indent=4))
    rs = requests.post(url, data=data)

    print(rs.text)
    print (rs.url)

quectel()
