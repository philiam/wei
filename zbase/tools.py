# coding: utf-8
"""
    这个工具组是专门为django应用提供 输出自己的log日志（couchdb, stdout），及修改内部编码方式用的
"""
from __future__ import unicode_literals

__author__ = 'philiam'
import logging
import couchdblogger
import os
import config as settings
import json
import urllib2
import time


def to_echo(obj):
    return json.dumps(obj, ensure_ascii=False)


def to_dict(o):
    """
    对象转换成字典型，目前只适合单层对象
    :param o:
    :return:
    """
    d = {}
    for k in dir(o):
        if not k.startswith('_'):
            d[k] = getattr(o, k)
    return d


def get_http_string(url, data=None):
    import urllib2

    try:
        log.debug('请求地址：%s' % url)
        log.debug('post数据: %s' % data)
        req = urllib2.Request(url, data=data)
        # google浏览器的 User-Agent
        req.add_header('User-Agent',
                       'Mozilla/5.0 (Windows NT 6.1; WOW64) '
                       'AppleWebKit/537.36 (KHTML, like Gecko) '
                       'Chrome/45.0.2454.93 Safari/537.36')
        resp = urllib2.urlopen(req)

        rs = resp.read()
        log.debug('返回数据：%s' % rs)
        return rs
    except Exception, ex:
        log.error(ex)
        return None


def get_program_name():
    """
        获取应用程序名称，如果settings.py中定义了 PROGRAM_NAME
        就用PROGRAM_NAME的值
    :return:
    """
    if 'PROGRAM_NAME' in dir(settings):
        pro = settings.PROGRAM_NAME
        if pro:
            return pro
    basename = os.path.basename(os.path.dirname(os.path.dirname(__file__)))
    return basename


class MyCouchDBLog(couchdblogger.CouchDBLogHandler):
    """
    自定义 couchdb log 的输出格式
    """

    def format(self, record):
        ts = record.asctime + str(record.created)[10:]
        return json.dumps(dict(
            message=record.getMessage(),
            level=record.levelname[:1],
            # created=record.asctime,
            created=ts,
            logger=record.name
        ))


def initlog():
    zlog = logging.getLogger(get_program_name())
    # print zlog
    sh = logging.StreamHandler()
    sh.setFormatter(logging.Formatter("[%(asctime)s-%(name)s %(levelname)-5s]: %(message)s",
                                      datefmt='%Y-%m-%d %H:%M:%S'))
    zlog.addHandler(sh)
    zlog.setLevel(logging.DEBUG)
    try:
        rp = urllib2.urlopen('http://localhost:5984')
        rs = rp.read()
    except:
        rs = ''
    if rs:
        # couchdb log
        h = MyCouchDBLog()
        h.setFormatter(logging.Formatter("%(message)s"))
        zlog.addHandler(h)
        zlog.setLevel(logging.DEBUG)
    else:
        zlog.warning('Couchdb is not available !')
    zlog.info("log init")
    return zlog


log = initlog()


def get_now_string():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))


# ===========================================
# ===========================================
# 使字符串的内部缺省编码为utf-8
def use_unicode():
    import sys

    reload(sys)
    sys.setdefaultencoding('utf8')





