;(function(window) {

var svgSprite = '<svg>' +
  ''+
    '<symbol id="icon-check1" viewBox="0 0 1024 1024">'+
      ''+
      '<path d="M56.888889 113.498413 56.888889 113.498413 56.888889 910.501586C56.888889 941.786163 82.21918 967.111111 113.498413 967.111111L910.501586 967.111111C941.786163 967.111111 967.111111 941.780821 967.111111 910.501586L967.111111 113.498413C967.111111 82.213839 941.780821 56.888889 910.501586 56.888889L113.498413 56.888889C82.213839 56.888889 56.888889 82.21918 56.888889 113.498413L56.888889 113.498413ZM391.320443 679.807858 149.961328 438.448741 69.50829 518.901777 391.320443 840.713933 451.660221 780.374153 932.143644 299.890731 851.690604 219.437693 391.320443 679.807858ZM0 113.498413C0 50.81497 50.780319 0 113.498413 0L910.501586 0C973.18503 0 1024 50.780319 1024 113.498413L1024 910.501586C1024 973.18503 973.219681 1024 910.501586 1024L113.498413 1024C50.81497 1024 0 973.219681 0 910.501586L0 113.498413Z"  ></path>'+
      ''+
    '</symbol>'+
  ''+
    '<symbol id="icon-check0" viewBox="0 0 1024 1024">'+
      ''+
      '<path d="M56.888889 113.498413 56.888889 113.498413 56.888889 910.501586C56.888889 941.786163 82.21918 967.111111 113.498413 967.111111L910.501586 967.111111C941.786163 967.111111 967.111111 941.780821 967.111111 910.501586L967.111111 113.498413C967.111111 82.213839 941.780821 56.888889 910.501586 56.888889L113.498413 56.888889C82.213839 56.888889 56.888889 82.21918 56.888889 113.498413L56.888889 113.498413ZM0 113.498413C0 50.81497 50.780319 0 113.498413 0L910.501586 0C973.18503 0 1024 50.780319 1024 113.498413L1024 910.501586C1024 973.18503 973.219681 1024 910.501586 1024L113.498413 1024C50.81497 1024 0 973.219681 0 910.501586L0 113.498413 0 113.498413Z"  ></path>'+
      ''+
    '</symbol>'+
  ''+
    '<symbol id="icon-radio0" viewBox="0 0 1024 1024">'+
      ''+
      '<path d="M512 0C229.2 0 0 229.2 0 512c0 282.8 229.2 512 512 512 282.8 0 512-229.2 512-512C1024 229.2 794.8 0 512 0zM512 977.5C254.9 977.5 46.5 769.1 46.5 512S254.9 46.5 512 46.5c257.1 0 465.5 208.4 465.5 465.5S769.1 977.5 512 977.5z"  ></path>'+
      ''+
    '</symbol>'+
  ''+
    '<symbol id="icon-radio1" viewBox="0 0 1024 1024">'+
      ''+
      '<path d="M512 0C229.2224 0 0 229.2224 0 512c0 282.7776 229.2224 512 512 512s512-229.2224 512-512C1024 229.2224 794.7776 0 512 0zM512 972.8c-254.5152 0-460.8-206.2848-460.8-460.8 0-254.5152 206.2848-460.8 460.8-460.8s460.8 206.2848 460.8 460.8C972.8 766.5152 766.5152 972.8 512 972.8zM512 512m-256 0a5 5 0 1 0 512 0 5 5 0 1 0-512 0Z"  ></path>'+
      ''+
    '</symbol>'+
  ''+
'</svg>'
var script = function() {
    var scripts = document.getElementsByTagName('script')
    return scripts[scripts.length - 1]
  }()
var shouldInjectCss = script.getAttribute("data-injectcss")

/**
 * document ready
 */
var ready = function(fn){
  if(document.addEventListener){
      document.addEventListener("DOMContentLoaded",function(){
          document.removeEventListener("DOMContentLoaded",arguments.callee,false)
          fn()
      },false)
  }else if(document.attachEvent){
     IEContentLoaded (window, fn)
  }

  function IEContentLoaded (w, fn) {
      var d = w.document, done = false,
      // only fire once
      init = function () {
          if (!done) {
              done = true
              fn()
          }
      }
      // polling for no errors
      ;(function () {
          try {
              // throws errors until after ondocumentready
              d.documentElement.doScroll('left')
          } catch (e) {
              setTimeout(arguments.callee, 50)
              return
          }
          // no errors, fire

          init()
      })()
      // trying to always fire before onload
      d.onreadystatechange = function() {
          if (d.readyState == 'complete') {
              d.onreadystatechange = null
              init()
          }
      }
  }
}

/**
 * Insert el before target
 *
 * @param {Element} el
 * @param {Element} target
 */

var before = function (el, target) {
  target.parentNode.insertBefore(el, target)
}

/**
 * Prepend el to target
 *
 * @param {Element} el
 * @param {Element} target
 */

var prepend = function (el, target) {
  if (target.firstChild) {
    before(el, target.firstChild)
  } else {
    target.appendChild(el)
  }
}

function appendSvg(){
  var div,svg

  div = document.createElement('div')
  div.innerHTML = svgSprite
  svg = div.getElementsByTagName('svg')[0]
  if (svg) {
    svg.setAttribute('aria-hidden', 'true')
    svg.style.position = 'absolute'
    svg.style.width = 0
    svg.style.height = 0
    svg.style.overflow = 'hidden'
    prepend(svg,document.body)
  }
}

if(shouldInjectCss && !window.__iconfont__svg__cssinject__){
  window.__iconfont__svg__cssinject__ = true
  try{
    document.write("<style>.svgfont {display: inline-block;width: 1em;height: 1em;fill: currentColor;vertical-align: -0.1em;font-size:16px;}</style>");
  }catch(e){
    console && console.log(e)
  }
}

ready(appendSvg)


})(window)
