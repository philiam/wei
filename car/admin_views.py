#!/bin/env python
# coding: utf-8
# author: ZhangTao
# Date  : 2016/11/3
from __future__ import unicode_literals


from django.shortcuts import render_to_response, HttpResponseRedirect, HttpResponse, Http404
from django.template import RequestContext

from core.models import *
from car.models import *
from zbase.tools import log, to_echo
import urllib2
import json
import traceback
from wei.settings import gl
from core.QR import get_access_token
import requests
from django.shortcuts import render
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required

from django.contrib.auth import login

from .models import Firmware
from django.views.decorators.csrf import csrf_exempt
from core.mqtt_center import mq

@staff_member_required
def file_select(request):
    cmd = {
        "Act": "Upd",
        "MD5": "",
        "Path": "211.154.146.70:21,obd,obd888,/v/,vcan0111",
        "When": "2"
    }
    imei = request.GET.get('imei', '')
    context = {
        'has_permission': True,
        'imei': imei,
        'cmd': json.dumps(cmd,indent=4),
        'firmwares': Firmware.objects.all()
    }
    # context = {}
    # context['firmwares'] = Firmware.objects.all()
    return render(request, 'file_select.html', context)


def tlogin(request):
    # context = {'has_permission': True}
    u = UserProfile.objects.get(openid="oVYpbuL6B0Z-8H6q9IGQEwYObr6Q")
    login(request, u)
    context = {}
    return render(request, 't_1.html', context)


@csrf_exempt
def query_imei(request):
    q = request.GET.get('q', '')
    # context = {}
    rs = Device.objects.filter(imei__icontains=q)[:10]
    imei_list =[]
    for i in rs:
        imei_list.append(i.imei)
    return HttpResponse(json.dumps(imei_list), content_type="text/plain")


@csrf_exempt
def load_firmware(request):
    q = request.GET.get('q', '')
    # context = {}

    # 多字段模糊检索
    rs = Firmware.objects.filter(Q(file_path__icontains=q) |
                                 Q(version__icontains=q))[:10]
    # print(len(rs))
    content = {'items': rs}
    return render(request, 'firmware.html', content)


def pub_upd(request):
    if request.method == 'POST':
        imei = request.POST.get('imei')
        pk = request.POST.get('pk')
        dev = Firmware.objects.get(pk=pk)
        ftp_user = "211.154.146.70:21,obd,obd888"
        fn = dev.file_path.name.split('/')
        cmd = {
            "Act": "Upd",
            "MD5": dev.md5,
            "Path": "%s,/%s/,%s" % (ftp_user, '/'.join(fn[:-1]), fn[-1]),
            "When": "2"
        }
        rs = {'cmd': json.dumps(cmd, indent=4), 'ct': time.strftime('%Y-%m-%d %H:%M:%S')}
        # 发送升级指令
        mq.publish(imei, json.dumps(cmd))
        return HttpResponse(json.dumps(rs), content_type="text/json")
    else:
        raise Http404('木有啦！')