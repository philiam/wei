#coding:utf-8
from django.contrib import admin
from car.models import *
# Register your models here.
import random
from zbase.tools import get_http_string
from wei.settings import gl

def genAuthCode(length):
    seed = '1234567890abcdefghijklmnopqrst'
    sa = []
    for i in range(length):
        sa.append(random.choice(seed))
    return ''.join(sa)


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('openid', 'name',
                    # 'cellphone', 'email',
                    'vehicle', 'vehicles',
                    'district',
                    # 'emergency_contact', 'emergency_phone',
                    'date_created')
    search_fields = ('name', 'cellphone', 'vehicle__imei')
    # list_filter = ('imei',)

    # 编辑项
    # fieldsets = (
    #     (None, {'fields': ('imei', 'image')}),
    # )
admin.site.register(UserProfile, UserProfileAdmin)


class DeviceAdmin(admin.ModelAdmin):
    list_display = ('imei', 'data_log', 'auth_code', 'qrcode', 'date_created', 'op')
    search_fields = ('imei', 'auth_code','last_data')
    # list_filter = ('imei',)
    # fieldsets = (
    #     (None, {'fields': ('imei')}),
    # )

    def op(self, obj):
        return '<a href="/car/fs?imei=%s" class="btn btn-warn"> 固件升级 </a>'% obj.imei
    op.allow_tags = True
    op.short_description = '操作'

    def data_log(self, obj):
        url2 = 'http://gards.covixon.com:9600/gardsdb/_design/q1/_view/cct_p?limit=50&' \
               'startkey=["%s","3"]&' \
               'endkey=["%s","2"]&descending=true' % (obj.imei, obj.imei)
        return ' <a href=\'%s\' > 报文查看 </a>' % url2
    data_log.allow_tags = True
    data_log.short_description = '报文'

    def save_model(self, request, obj, form, change):
        # 自定义操作
        if obj.auth_code.strip() == '':
            obj.auth_code = genAuthCode(6)

        if obj.imei:
            appid = 'wx4806b076e959f03f'
            url = 'http://%s:%s/api/qrcode/%s/%s' % (gl['wx']['host'], gl['wx']['port'], appid, obj.imei)
            qr = get_http_string(url)
            obj.qr = qr
        obj.save()

admin.site.register(Device, DeviceAdmin)


class SimCardAdmin(admin.ModelAdmin):
    list_display = ('iccid', 'isp', 'imei', 'number', 'date_created')
    search_fields = ('iccid', 'number')
    readonly_fields = ('imei',)

admin.site.register(SimCard, SimCardAdmin)


class FirmwareAdmin(admin.ModelAdmin):
    list_display = ('date_created', 'file_path', 'version', 'md5')
    search_fields = ('file_path', 'version')
    readonly_fields = ('md5',)
admin.site.register(Firmware, FirmwareAdmin)
