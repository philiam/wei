# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from zbase.tools import *
import traceback
from core.QR import get_access_token
import json
from wei import settings
import os
import hashlib


class Device(models.Model):
    imei = models.CharField(_('imei'), max_length=20, unique=True)
    last_data = models.TextField(_('last data group'), null=True, blank=True)
    auth_code = models.CharField(_('auth code'), max_length=20, null=True, blank=True)
    date_created = models.DateTimeField(_('Date Created'), auto_now_add=True, blank=True)
    qr = models.CharField(_('QR Code'), max_length=500, null=True, blank=True)

    class Meta:
        verbose_name = _('Device')
        verbose_name_plural = _('Devices')
        db_table = 'cx_device'

    def qrcode(self):
        if self.qr:
            return '<a href="%s" target=_blank>查看</a>' % self.qr

    qrcode.allow_tags = True
    qrcode.short_description = _('QR Code')

    def __unicode__(self):
        return self.imei


class UserProfile(models.Model):
    openid = models.CharField(_('openid'), max_length=50, null=True, blank=True)
    name = models.CharField(_('name'), max_length=50, null=True, blank=True, default='')
    age = models.IntegerField(_('age'), null=True, default=20)
    cellphone = models.CharField(_('cellphone'), max_length=20, null=True, blank=True, default='')
    email = models.CharField(_('email'), max_length=50, null=True, blank=True, default='')
    district = models.CharField(_('district'), max_length=100, null=True, blank=True, default='')
    emergency_contact = models.CharField(_('emergency contact'), max_length=50, null=True, blank=True, default='')
    emergency_phone = models.CharField(_('emergency phone'), max_length=50, null=True, blank=True, default='')
    date_created = models.DateTimeField(_('Date Created'), auto_now_add=True, blank=True)
    vehicle = models.ForeignKey(Device, max_length=20, null=True, blank=True, default='')
    vehicles = models.TextField(_('vehicles'), null=True, blank=True)
    # 记录微信用户的实时位置, 语音导航的时候需要使用
    last_addr = models.TextField(_('last city'), null=True, blank=True, default='')
    last_time = models.DateTimeField(_('timestamp'), null=True, auto_now_add=True)

    class Meta:
        verbose_name = _('User Profile')
        verbose_name_plural = _('User Profiles')
        db_table = 'cx_user'

    def __unicode__(self):
        return self.name

    # 保存时触发
    def save(self, *args, **kwargs):
        log.debug('model save %s' % self.name)
        if not self.name and self.openid:
            appid = 'wx4806b076e959f03f'
            try:
                ac = get_access_token(appid)
                url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s&lang=zh_CN' % (
                ac, self.openid)
                # url = 'http://localhost:8000/api/qrcode/%s/%s' % (appid, obj.imei)
                rs = get_http_string(url)
                jc = json.loads(rs)
                if 'errcode' not in jc:
                    if jc['subscribe'] == 1:
                        self.name = jc['nickname']
                        self.district = '%s %s %s' % (jc['country'], jc['province'], jc['city'])
            except:
                log.error(traceback.format_exc())
        super(self.__class__, self).save(*args, **kwargs)


# sim 卡管理
class SimCard(models.Model):
    ISP_CHOICES = (
        (u'0', u'未知'),
        (u'1', u'移云通信'),
        (u'2', u'畅行神州'),
    )

    iccid = models.CharField(_('iccid'), max_length=20, null=True, blank=True)
    number = models.CharField(_('Number'), max_length=15, null=True, blank=True)
    date_created = models.DateTimeField(_('Date Created'), auto_now_add=True)
    imei = models.CharField(_('imei'), max_length=20, null=True, blank=True)
    isp = models.CharField(_(u'接入商'), max_length=20, choices=ISP_CHOICES, null=True, blank=True)

    class Meta:
        verbose_name = _('Sim Card')
        verbose_name_plural = _('Sim Card')
        db_table = 'wx_sim_card'
        # ordering = ['-id']

    def __unicode__(self):
        return self.number


# 动态指定文件保存路径
def firm_dir(instance, filename):
    # file will be uploaded to MEDIA_ROOT/wx/[sub_name]/
    if filename.startswith('main'):
        sub_name = 'm/'
    elif filename.startswith('vcan'):
        sub_name = 'v/'
    else:
        sub_name = 'x'
    return 'wx/%s/%s' % (sub_name, filename)


class Firmware(models.Model):
    md5 = models.CharField(_('MD5'), max_length=50, null=True, blank=True, default='将自动计算')
    date_created = models.DateTimeField(_('Date Created'), auto_now_add=True, blank=True)
    file_path = models.FileField(_('file path'),  max_length=500, upload_to=firm_dir)
    version = models.CharField(_('version'), max_length=20, null=True, blank=True, default='1.0')

    class Meta:
        verbose_name = _('Firmware')
        verbose_name_plural = _('Firmware')
        db_table = 'wx_firmware'
        # ordering = ['-id']

    def __unicode__(self):
        return self.file_path.name

        # 保存时触发
    def save(self, *args, **kwargs):
        log.debug('Firmware md5 calculate and save %s' % self.file_path.path)

        # print (self.file_path.read())
        m = hashlib.md5()
        fc = self.file_path.read()
        m.update(fc)
        self.md5 = m.hexdigest()
        if os.path.exists(self.file_path.path):
            # 修改本条数据的时候
            log.debug('%s exists' % self.file_path.path)
        else:
            # 添加数据的时候
            log.warn('file %s  does not exist !' % self.file_path.path)

        super(self.__class__, self).save(*args, **kwargs)


