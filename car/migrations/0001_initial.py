# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('imei', models.CharField(max_length=20, verbose_name='imei')),
                ('last_data', models.TextField(null=True, verbose_name='last data group', blank=True)),
                ('auth_code', models.CharField(max_length=20, null=True, verbose_name='auth code', blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Date Created')),
                ('qr', models.CharField(max_length=500, null=True, verbose_name='QR Code', blank=True)),
            ],
            options={
                'db_table': 'cx_device',
                'verbose_name': 'Device',
                'verbose_name_plural': 'Devices',
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('openid', models.CharField(max_length=50, null=True, verbose_name='openid', blank=True)),
                ('name', models.CharField(default=b'', max_length=50, null=True, verbose_name='name', blank=True)),
                ('age', models.IntegerField(default=20, null=True, verbose_name='age')),
                ('cellphone',
                 models.CharField(default=b'', max_length=20, null=True, verbose_name='cellphone', blank=True)),
                ('email', models.CharField(default=b'', max_length=50, null=True, verbose_name='email', blank=True)),
                ('district',
                 models.CharField(default=b'', max_length=100, null=True, verbose_name='district', blank=True)),
                ('emergency_contact',
                 models.CharField(default=b'', max_length=50, null=True, verbose_name='emergency contact', blank=True)),
                ('emergency_phone',
                 models.CharField(default=b'', max_length=50, null=True, verbose_name='emergency phone', blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Date Created')),
                ('vehicle', models.ForeignKey(default=b'', blank=True, to='car.Device', max_length=20, null=True)),
            ],
            options={
                'db_table': 'cx_user',
                'verbose_name': 'User Profile',
                'verbose_name_plural': 'User Profiles',
            },
        ),
    ]
