# coding: utf-8

from django.shortcuts import render_to_response, HttpResponseRedirect, HttpResponse, Http404, render
from django.template import RequestContext

from core.models import *
from car.models import *
from zbase.tools import log, to_echo
import urllib2
import json
import traceback
from wei.settings import gl
from core.QR import get_access_token
import requests
from django.contrib.admin.views.decorators import staff_member_required


def test(request):
    context = {}
    return render(request, 'test.html', context)


# 关联车辆
def bind(request):
    if request.method == "GET":
        openid = request.GET.get('openid', '')
        su = UserProfile.objects.filter(openid=openid)

        if su.__len__() > 0 and su[0].vehicle:
            u = su[0]
            context = {"openid": openid, "user": u}
            log.debug(context)
            return HttpResponseRedirect('/car/hardware?openid=%s' % openid)
            # return render(request, 'relation_out.html', context)
        else:
            u = None

            # 扫描的回调的时候
            imei = request.GET.get('imei', '')
            if imei:
                context = {"imei": imei, "openid": openid}
                return render(request, 'bind.html', context)

            # 点击菜单关联
            context = {"openid": openid, "user": u}
            log.debug(context)
            return render(request, 'relation.html', context)

    elif request.method == "POST":
        openid = request.GET.get('openid', '')
        imei = request.POST.get('imei', '')
        auth_code = request.POST.get('auth_code', '')
        # print(request.user)
        # request.user = 'a'
        log.debug("openid=%s, imei=%s, auth_code=%s" % (openid, imei, auth_code))

        dev = Device.objects.filter(imei=imei)

        if len(dev) > 0 and dev[0].auth_code == auth_code:
            u = UserProfile.objects.get_or_create(openid=openid)[0]
            u.vehicle = dev[0]
            # 同时在当前用户的关联列表中添加一条
            if not u.vehicles or u.vehicles == '[]':
                vcs = [u.vehicle.imei]
            else:
                vcs = json.loads(u.vehicles)
                vcs.append(u.vehicle.imei)
            # vcs = u.vehicles + "," + u.vehicle.imei if u.vehicles else u.vehicle.imei
            u.vehicles = json.dumps(vcs)
            u.save()
            context = {"openid": openid, "user": u}
            log.debug(context)
            return HttpResponseRedirect('/car/hardware?openid=%s' % openid)
            #return render(request, 'relation_out.html', context)
        else:

            context = {"openid": openid, "msg": '授权失败!'}
            log.debug(to_echo(context))
            return render(request, 'msg.html', context)


#取消关联
def unbind(request):
    if request.method == "POST":
        openid = request.POST['openid']
        active_code = request.POST['active_code']
        su = UserProfile.objects.filter(openid=openid)

        if su.__len__() > 0 and su[0].vehicle:
            u = su[0]
            context = {"openid": openid, "msg": '关联设备[%s] 已解除关联！' % u.vehicle.imei}
            # 从关联的列表中删除一个imei
            vcs = json.loads(u.vehicles)
            vcs.remove(u.vehicle.imei)
            u.vehicles = json.dumps(vcs)
            u.vehicle = None

            u.save()

            log.debug(to_echo(context))
            return render(request, 'msg.html', context)
            # return render(request, 'relation_out.html', context)
        else:
            context = {"openid": openid, "msg": '还没关联车辆！'}
            log.debug(context)
            return render(request, 'msg.html', context)
    else:
        raise Http404


def location(request):
    openid = request.GET.get('openid', '')
    try:
        u = UserProfile.objects.get(openid=openid)

        if not u.vehicle:
            u = UserProfile.objects.get(openid='test')

        if u.vehicle:
            jlast = get_last(u.vehicle)
            gps = []
            if 'lat' in jlast:
                gps.append(jlast['lon']['value'])
                gps.append(jlast['lat']['value'])
                gps.append(jlast['lat']['created'][5:-3])

            context = {
                'gps': json.dumps(gps)
            }

            log.debug('对比本地数据库分析提取最新 经、纬度 ： %s' % to_echo(context))
            return render(request, 'map.html', context)
            # log.debug(json.dumps(jret, indent=4, ensure_ascii=False))
        else:
            context = {
                'msg': '还没有关联车辆 !'
            }
            log.warn(to_echo(context))
            return render(request, 'msg.html', context)
    except:
        log.error(traceback.format_exc())
        context = {
            'msg': '还没有关联车辆 !'
        }
        log.warn(to_echo(context))
        return render(request, 'msg.html', context)


# 车辆状态
def status(request):
    openid = request.GET.get('openid', '')
    try:
        u = UserProfile.objects.get(openid=openid)

        if not u.vehicle:
            u = UserProfile.objects.get(openid='test')

        if u.vehicle:
            jlast = get_last(u.vehicle)
            car = {
                'batvolt': '--',
                'fuel_lvl': '--',
                'fuel_qty': '--',
                'hand_brake': '--',
                'key': '--',
                'acc': '--',
                'clt_temp': '--',
                'clt_t': '--', # 改版 冷却液温度
                'eng': '--',
                'gear': '--',
                'rpm': '--',

                # 行程时间
                'trip_time': '--',
                #2.里程
                'odo': '--',
                #车速
                'vspd': '--',


                #中控锁
                'clock': '--',
                # 车门
                'door': '--',
                'door_tk': '--',
                'door_lf': '--',
                'door_rf': '--',
                'door_lr': '--',
                'door_rr': '--',

                # 车窗
                'wnd': '--',
                'wnd_lf': '--',
                'wnd_rf': '--',
                'wnd_lr': '--',
                'wnd_rr': '--',

                # 安全带
                'belt': '--',
                'belt_r': '--',
                'belt_l': '--',
                # 车灯
                'light': '--',
                # {"hzd":0/1}        //hazard
                # {"pos":0/1}        //positioin
                # {"beam_l":0/1}    //beam low
                # {"beam_h":0/1}    //beam high
                # {"fog_f":0/1}    //fog front
                # {"fog_r":0/1}    //fog rear
                # {"turn_l":0/1}    //turn left
                # {"turn_r":0/1}    //turn right

                'mdm_imei': '--',
            }
            for k in car:
                if k in jlast:
                    car[k] = jlast[k]['value']

            log.debug('分析提取的数据：%s' % to_echo(car))

            # 安全带
            # if car['belt'] != '--':
            # xx = car['belt']
            #     belt = {}
            #     if xx.__contains__('LF1'):
            #         belt['LF'] = '已系'
            #     else:
            #         belt['LF'] = '未系'
            #
            #     if xx.__contains__('RF1'):
            #         belt['RF'] = '已系'
            #     else:
            #         belt['RF'] = '未系'
            #
            #     car['belt'] = belt
            #
            # if car['door'] != '--':
            #     xx = car['door']
            #     d = {}
            #     if xx.__contains__('LF1'):
            #         d['LF'] = '开'
            #     else:
            #         d['LF'] = '关'
            #
            #     if xx.__contains__('RF1'):
            #         d['RF'] = '开'
            #     else:
            #         d['RF'] = '关'
            #
            #     if xx.__contains__('LR1'):
            #         d['LR'] = '开'
            #     else:
            #         d['LR'] = '关'
            #
            #     if xx.__contains__('RR1'):
            #         d['RR'] = '开'
            #     else:
            #         d['RR'] = '关'
            #
            #     if xx.__contains__('TK1'):
            #         d['TK'] = '开'
            #     else:
            #         d['TK'] = '关'
            #
            #     car['door'] = d
            #
            # if car['wnd'] != '--':
            #     xx = car['wnd']
            #     d = {}
            #     if xx.__contains__('LF1'):
            #         d['LF'] = '开'
            #     else:
            #         d['LF'] = '关'
            #
            #     if xx.__contains__('RF1'):
            #         d['RF'] = '开'
            #     else:
            #         d['RF'] = '关'
            #
            #     if xx.__contains__('LR1'):
            #         d['LR'] = '开'
            #     else:
            #         d['LR'] = '关'
            #
            #     if xx.__contains__('RR1'):
            #         d['RR'] = '开'
            #     else:
            #         d['RR'] = '关'
            #
            #     car['wnd'] = d

            # 中控锁
            if car['clock'] != '--':
                if car['clock'] == 'LOCK':
                    car['clock'] = '上锁'
                else:
                    car['clock'] = '解锁'

            if car['key'] == 'OUT':
                car['key'] = '已拔出'

            # 油量
            # 如果有fuel_qty，这个是油量 610除以10，就是60升
            # 如果fuel_qty如果没有，再使用fuel_lvl这个百分比
            if car['fuel_qty'] != '--' and car['fuel_qty'] != 65535:
                car['fuel'] = '%s 升' % (car['fuel_qty']/10)
                if car['fuel_qty'] >= 800:
                    car['fuel'] = '--'
                if car['fuel_qty'] == 0:
                    car['fuel'] = '--'
            elif car['fuel_lvl'] != '--' and car['fuel_lvl'] != 255:
                car['fuel'] = '%s %%' % car['fuel_lvl']
            else:
                car['fuel'] = '--'

            if car['hand_brake'] != '--':
                if car['hand_brake'] == 'PULL':
                    car['hand_brake'] = '拉起'
                else:
                    car['hand_brake'] = '放下'

            # 兼容以前的 clt_temp 处理
            if car['clt_t'] != '--':
                car['clt_temp'] = car['clt_t']
            if car['clt_temp'] != '--':
                if car['clt_temp'] == 0 or car['clt_temp'] == 255:
                    car['clt_temp'] = '--'

            # 副驾暂不支持
            car['belt_r'] = '--'
            context = {
                'car': car
            }

            log.debug('再次处理后的数据：%s' % to_echo(context))
            return render(request, 'status.html', context)
        else:
            context = {
                'msg': '还没有关联车辆 !'
            }
            log.warn(to_echo(context))
            return render(request, 'msg.html', context)
    except:
        log.error(traceback.format_exc())
        context = {
            'msg': '还没有关联车辆 !'
        }
        log.warn(to_echo(context))
        return render(request, 'msg.html', context)


# 获取所有最新数据
def get_last(v=None, imei=None):
    # 获取最新数据
    if v:
        url = "http://%s:%s/api/gards_query/terminal/%s/mosaic" % (
        gl['gards_api']['host'], gl['gards_api']['port'], v.imei)

        log.info('发起api请求')
        log.debug('请求地址：%s' % url)
        resp = urllib2.urlopen(url)
        result = resp.read()
        log.info('收到请求返回')
        # log.debug('原始数据： %s' % result)
        jret = json.loads(result)
        if v.last_data:
            jlast = json.loads(v.last_data)
        else:
            jlast = {}

        log.info('提取并保存最新拼接字段到数据库')
        if 'mosaic' in jret:
            for k in jret['mosaic']:
                jlast[k] = jret['mosaic'][k]
            v.last_data = to_echo(jlast)
            v.save()
        return jlast

    return None


# def get_last(v):
#     if v:
#         client = v.imei
#         rs = {}
#         mosaic = sr.hgetall('last:m:' + client)
#         print(type(mosaic))
#         ts = sr.hgetall('last:t:' + client)
#         for k, v in mosaic.items():
#             print(k, v)
#             rs[k] = {"value": v, "created": ts[k]}
#
#         # ret = {"ret": "0", "rs": rs, "msg": ""}
#         return rs
#     return None


def hardware(request):
    openid = request.GET.get('openid', '')
    try:
        u = UserProfile.objects.get(openid=openid)
        if u.vehicle:
            jlast = get_last(u.vehicle)
            hw = {'fw': '--', 'fw_can': '--', 'fw_app': '--', 'sim_ccid': '--', 'sim_imsi': '--', 'mdm_imei': '--', 'vin': '--'}
            for k in hw:
                if k in jlast:
                    hw[k] = jlast[k]['value']
            context = {
                'openid': openid,
                'hw': hw,
                'imei': u.vehicle.imei,
                'imei_list': u.vehicles
            }
            log.debug('提取的数据：%s' % to_echo(context))
            return render(request, 'hardware.html', context)
        else:
            context = {
                'msg': '还没有关联车辆 !'
            }
            log.warn(to_echo(context))
            return render(request, 'msg.html', context)
    except:
        log.error(traceback.format_exc())
        context = {
            'msg': '还没有关联车辆 !'
        }
        log.warn(to_echo(context))
        return render(request, 'msg.html', context)


def oauth2_url(menu_id):
    encoded_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx4806b076e959f03f&redirect_uri=http%3a%2f%2fgards.covixon.com%2fapi%2foauth2%2f" + menu_id + "%2f&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect"
    return encoded_url


def create_menu(request):
    url = 'https://api.weixin.qq.com/cgi-bin/menu/create?access_token=%s' % get_access_token()
    data = {
     "button":[
      {
        "name":"车辆",
        "sub_button":[
        {
           "type":"view",
           "name":"关联车辆",
           "url": oauth2_url('11')
        },
        {
           "type":"view",
           "name":"车辆位置",
           "url": oauth2_url('12')
        },
        {
           "type": "view",
           "name": "车辆状态",
           "url": oauth2_url('13')
        },
        {
           "type": "click",
           "name": "最新故障",
           "key": "14"
        }
        ]
    },
    {
        "name":"车主",
        "sub_button":[
        {
          "type":"view",
          "name":"宝典",
          "url": oauth2_url('21')
        }
        ]
    },
    {
        "name": "服务",
        "sub_button": [
            {
               "type": "view",
               "name": "凯越电子",
               "url":"http://www.kaiyuegroup.com.cn/"
            },
            {
               "type": "view",
               "name": "康松科技",
               "url":"http://www.covixon.com/"
            }                
        ]
    }]
}
    # rs = get_http_string(url, json.dumps(data, ensure_ascii=False))
    r = urllib2.urlopen(url, json.dumps(data, ensure_ascii=False))
    rs = r.read()
    log.debug(rs)
    return HttpResponse(rs)


def ac(request):
    rac = get_access_token()
    rs = {'ac': rac}
    return HttpResponse(to_echo(rs))


def pub_sms(imei):
    try:
        log.debug('查询iccid')
        jlast = get_last(Device.objects.get(imei__iexact=imei))
        if 'sim_ccid' in jlast:
            iccid = jlast['sim_ccid']['value']
            log.debug('iccid=%s' % iccid)
            sc = SimCard.objects.get(iccid=iccid)

            num = sc.number
            log.debug('phone number：%s' % num)
            # url = "http://simdx.cxsz.com.cn/ShortMessage/shortm/sendAutoMessage.do" \
            #             "?user=yikesms&pwd=1hkaki6j&mobile=%s&content=*awaken*;" % num
            # 重复发三次
            # log.debug('发送唤醒短信3次')
            # quectel(num, '*awaken*;')
            return quectel(num, '*awaken*;')
    except:
        log.error(traceback.format_exc())


# 改用新短信接口
def quectel(num='1064868406007', content=''):
    url = 'http://139.196.41.136:8098/openapi/router'
    data = {
        'appkey': '5d428b3263',
        't': int(time.time()),
        'method': 'fc.function.sms.send',
        'msisdns': num,
        'content': content
    }

    sorted_data = sorted(data.items(), key=lambda d: d[0])
    # print(sorted_data)
    app_secret = 'acdef0dfq'
    s = app_secret
    for k in sorted_data:
        s += '{}{}'.format(k[0], k[1])

    s += app_secret
    sign = hashlib.sha1(s).hexdigest()

    # print(s)
    data['sign'] = sign
    # import json
    # print (json.dumps(data,indent=4))
    rs = requests.post(url, data=data)

    return rs.text
    # print(rs.text)
    # print (rs.url)


def owner(request):
    openid = request.GET.get('openid', '')
    context = {}
    return render(request, 'owner.html', context)


@staff_member_required
def home(request):
    context = {'has_permission': True}
    return render(request, 'index.html', context)