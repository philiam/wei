# -+- coding: utf-8 -+-
#
# Created by philiam on 2015/12/8.
#
from __future__ import unicode_literals
from core.QR import get_access_token
from zbase.tools import *
from car.models import UserProfile

__author__ = 'philiam'


# 发送微信模板消息
def post_weixin(post_data):
    access_token = get_access_token()
    url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=%s" % access_token
    rs = get_http_string(url, json.dumps(post_data))


def send_voltage_msg(openid, volt, ob=''):
    # 模板样式
    #
    # {{first.DATA}}
    # 爱车状态：{{keyword1.DATA}}
    # 当前电压：{{keyword2.DATA}}
    # 发生时间：{{keyword3.DATA}}
    # {{remark.DATA}}
    first = '您的爱车电压过低, 请留意'
    volt = round(volt / 1000.0, 1)
    post_data = {
        "touser": openid,
        "template_id": "PsWZD3Z56tBEpxuYEySheaOfue_m8wuViyjNE4TU1ys",
        "url": "",
        "topcolor": "#FF0000",
        "data": {
            "first": {
                "value": first,
                "color": "#0000FF"
            },
            "keyword1": {
                "value": '电压过低',
                "color": "#0000FF"
            },
            "keyword2": {
                "value": str(volt) + '伏',
                "color": "#0000FF"
            },
            "keyword3": {
                "value": get_now_string(),
                "color": "#0000FF"
            },
            "remark": {
                "value": '',
                "color": "#00FF00"
            }
        }
    }
    post_weixin(post_data)


def batvolt_low_alarm(grid):
    users = UserProfile.objects.filter(vehicle__imei=grid['client'])
    for user in users:
        send_voltage_msg(user.openid, grid['batvolt'])


def send_leave_msg(openid, scene, client=''):
    # {{first.DATA}}
    # 报警车辆：{{keyword1.DATA}}
    # 报警时间：{{keyword2.DATA}}
    # 报警类型：{{keyword3.DATA}}
    # 详细情况：{{keyword4.DATA}}
    # {{remark.DATA}}
    first = '您好，您关注的车辆有报警'
    scenes = scene.split(',')

    # DLF1,DRF1,DLR1,DRR1 主驾车门，副驾车门，左后车门，右后车门
    # DHD1,DTK1 引擎盖，后备箱
    # WLF1,WRF1,WLR1,WRR1 四个车窗
    # HZD1,POS1,BML1,BMH1,FGF1,FGR1 危险报警灯，位置灯，近光灯，远光灯，前雾灯，后雾灯
    # GP0 gear-p，未置P档
    # PK0 parking, 未拉起手刹
    #
    # 门窗灯，是0 是正常，1是异常
    # 手刹，P档，因为行驶中0是正常，1是异常
    # 停车是，0是异常，1是正常

    door = []
    ds = ''
    if 'DLF1' in scenes:
        ds += '主驾,'
    if 'DRF1' in scenes:
        ds += '副驾,'
    if 'DLR1' in scenes:
        ds += '左后,'
    if 'DRR1' in scenes:
        ds += '右后,'

    # 车灯
    ls = ''
    if 'HZD1' in scenes:
        ls += '危险报警灯,'
    if 'POS1' in scenes:
        ls += '位置灯,'
    if 'BML1' in scenes:
        ls += '近光灯,'
    if 'BMH1' in scenes:
        ls += '远光灯,'
    if 'FGF1' in scenes:
        ls += '前雾灯,'
    if 'FGR1' in scenes:
        ls += '后雾灯,'

    # 车窗
    ws = ''
    if 'WLF1' in scenes:
        ws += '主驾,'
    if 'WRF1' in scenes:
        ws += '副驾,'
    if 'WLR1' in scenes:
        ws += '左后,'
    if 'WRR1' in scenes:
        ws += '右后,'

    # 其它
    oth = ''
    if 'DHD1' in scenes:
        oth += '引擎盖,'
    if 'DTK1' in scenes:
        oth += '后备箱,'
    # if 'GP0' in scenes:
    #     oth += '未置P档,'
    if 'PK0' in scenes:
        oth += ''  # '未拉起手刹,'

    if not ds:
        if not ls:
            if not oth:
                if not ws:
                    return
    # 合并
    detail = '锁车异常，'
    if ds:
        detail += '车门(%s),' % ds[:-1]
    if ws:
        detail += '车窗(%s),' % ws[:-1]
    if ls:
        detail += '车灯(%s),' % ls[:-1]
    if oth:
        detail += oth

    detail = detail.replace(',', '，')

    post_data = {
        "touser": openid,
        "template_id": "doK-7edTO7dHKp_pTxWlo5qrThtEQ42O3sDWwvJdW78",
        "url": "",
        "topcolor": "#FF0000",
        "data": {
            "first": {
                "value": first,
                "color": "#0000FF"
            },
            "keyword1": {
                "value": client,
                "color": "#0000FF"
            },
            "keyword2": {
                "value": get_now_string(),
                "color": "#0000FF"
            },
            "keyword3": {
                "value": '锁车报警',
                "color": "#0000FF"
            },
            "keyword4": {
                "value": detail[:-1],
                "color": "#0000FF"
            },
            "remark": {
                "value": '',
                "color": "#00FF00"
            }
        }
    }
    post_weixin(post_data)


def leave_alarm(grid):
    users = UserProfile.objects.filter(vehicle__imei=grid['client'])
    for user in users:
        send_leave_msg(user.openid, grid['leave_scene'], grid['client'])


# 其他报警处理，测试用
def default_alarm(grid):

    alarm_str = ''
    if grid['alarm'] == 'secing_door':
        alarm_str = "您的车门被非法打开!"
    elif grid['alarm'] == 'secing_hzd':
        alarm_str = "您的车内情况异常，请确定是否有乘客，儿童或宠物遗忘在车内!"

    if grid['alarm'] == 'idle2long':
        alarm_str = "长时间怠速，燃油燃烧不完全可能造成发动机积碳，气缸压力较低亦可能影响密封性，有损发动机寿命，并谨防有毒气体回流到车内"
    elif grid['alarm'] == 'idlesafe':
        alarm_str = "您的爱车怠速时间较长，请确保车内乘客安全，若已离开车辆，请确保发动机已熄火"

    if not alarm_str:
        return

    users = UserProfile.objects.filter(vehicle__imei=grid['client'])
    first = '您好，您关注的车辆有报警'
    for user in users:
            post_data = {
                "touser": user.openid,
                "template_id": "doK-7edTO7dHKp_pTxWlo5qrThtEQ42O3sDWwvJdW78",
                "url": "",
                "topcolor": "#FF0000",
                "data": {
                    "first": {
                        "value": first,
                        "color": "#0000FF"
                    },
                    "keyword1": {
                        "value": grid['client'],
                        "color": "#0000FF"
                    },
                    "keyword2": {
                        "value": get_now_string(),
                        "color": "#0000FF"
                    },
                    "keyword3": {
                        "value": '安全报警',
                        "color": "#0000FF"
                    },
                    "keyword4": {
                        "value": alarm_str,
                        "color": "#0000FF"
                    },
                    "remark": {
                        "value": '',
                        "color": "#00FF00"
                    }
                }
            }
            post_weixin(post_data)

