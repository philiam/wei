"""wei URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from car.views import *
# urlpatterns = [
#     url(r'^bind', 'car.views.bind'),
#     url(r'^unbind/', 'car.views.unbind', name='unbind'),
#     url(r'^location', 'car.views.location'),
#     url(r'^status', 'car.views.status'),
#     url(r'^hardware', 'car.views.hardware'),
#     url(r'^cm', 'car.views.create_menu'),
#     url(r'^ac', 'car.views.ac'),
# ]
from car.admin_views import file_select, tlogin, load_firmware, pub_upd

urlpatterns = [
    url(r'^bind', bind),
    url(r'^unbind/', unbind, name='unbind'),
    url(r'^location', location),
    url(r'^status', status),
    url(r'^hardware', hardware),
    url(r'^cm', create_menu),
    url(r'^ac', ac),
    url(r'^fs', file_select),
    url(r'^t', tlogin),
    url(r'^firmware', load_firmware),
    url(r'^upd', pub_upd),
    url(r'^owner', owner),
]
