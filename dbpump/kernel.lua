--
-- User: ZhangTao
-- Date: 2016/5/19
-- Time: 10:03
-- descript:
--      数据接收缓存分类处理（拼图、时间、最新报文）脚本
-- evalsha(sha, 1, ClientId, MsgId, Topic, Payload, QOS, group, category, created, ip)

local data = {
    client=KEYS[1],
    _id=ARGV[1],
    topic=ARGV[2],
    payload=ARGV[3],
    qos=ARGV[4],
    group=ARGV[5],
    category=ARGV[6],
    created=ARGV[7],
    ip=ARGV[8]
}

-- 将参数组合成json字符串 推入数据库通道
local ds = cjson.encode(data)
redis.call('RPUSH','dbs', ds)

-- 拆分拼接 payload 中的key:value 并缓存每个设备的拼图数据
-- local payload = cjson.decode(data['payload'])
-- 用 pcall 捕获非法json的异常
if ARGV[2] == 'r' then
    local status, payload = pcall(cjson.decode, ARGV[3])
    if status ~= true then
        redis.log(redis.LOG_WARNING,  "ZT:payload is not a json string ! ")
        return 1
    end
    -- 遍历payload json中的 每个key ,value
    for k,v in pairs(payload) do
        redis.call('HSET','last:m:'..KEYS[1], k, v)
        redis.call('HSET','last:t:'..KEYS[1], k, ARGV[7])
    end

    -- 缓存每个终端的最后一条数据
    redis.call('SET','grid:'..KEYS[1], ARGV[3])
    redis.call('SET','grid:t:'..KEYS[1], ARGV[7])

end

---- 缓存每个终端的最后一条数据
--redis.call('SET','grid:'..ARGV[2]..':'..KEYS[1], ARGV[3])
--redis.call('SET','grid:t:'..KEYS[1], ARGV[5])

return 0
