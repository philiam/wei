# coding:utf-8
import redis
import couchdb
import json
import traceback
import sys
import time
sys.path.append('..')
from zbase.tools import log

gl = {'redis_pw': 'cvx@9v6t',
      'current_db': 'gardsdb'
      }

couch = couchdb.Server()


# 从队列中取出、转换、并存储数据
def carrier():

    r = redis.StrictRedis(password=gl['redis_pw'])
    # 获取当前数据库
    # gl['current_db'] = r.get('current_db')
    # r2 = redis.StrictRedis(password=gl['redis_pw'], db=1)
    # msh = r.get('msha')
    v = None
    ret = True
    while True:
        if ret:
            # 如果上次写入失败，此次不pop数据，直接写上次的数据
            v = r.blpop('dbs')
            # r.script_load(msh)
        else:
            # 如果上次写入失败，等3s再写入
            time.sleep(3)

        ret = write(v[1])


# 保存到数据库
def write(data):
    try:
        # log.debug(data)
        db = couch[gl['current_db']]
        rs = json.loads(data)
        # rs['created'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(rs['created'])))
        db.save(rs)
        return True
    except:
        log.error(traceback.format_exc())
        return False

if __name__ == '__main__':
    carrier()
