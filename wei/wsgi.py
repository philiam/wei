"""
WSGI config for wei project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "wei.settings")
import sys

# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cxwx.settings")
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))


os.environ["DJANGO_SETTINGS_MODULE"] = "wei.settings"
os.environ['PYTHON_EGG_CACHE'] = '/tmp'
application = get_wsgi_application()
