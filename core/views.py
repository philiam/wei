# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, Http404
from django.views.decorators.csrf import csrf_exempt
from wechat_sdk import WechatBasic
from wechat_sdk.messages import (
    TextMessage, VoiceMessage, ImageMessage, VideoMessage, LinkMessage, LocationMessage, EventMessage
)
import json
from zbase.tools import log
import urllib2
from core.models import *
from core.QR import *
from robot import *
from mqtt_center import mq
from car.models import *
from car.views import get_last
import traceback
import re
from QR import get_access_token
import requests
import datetime
from sms import quectel

# wechat = WechatBasic(
# token='shfalian',
#     appid='wxae34c9c90bd90a6a',
#     appsecret='40916acc5119249b406f2280a43d0954'
# )
use_unicode()
wechat = WechatBasic()
# message = None

body = """
<xml>
<ToUserName><![CDATA[touser]]></ToUserName>
<FromUserName><![CDATA[fromuser]]></FromUserName>
<CreateTime>1405994593</CreateTime>
<MsgType><![CDATA[text]]></MsgType>
<Content><![CDATA[wechat]]></Content>
<MsgId>6038700799783131222</MsgId>
</xml>
"""


def main(request):
    return HttpResponseRedirect('http://gards.covixon.com:9600')


def mqtt_test(request):
    try:
        topic = 'r'
        msg = request.REQUEST.get('m', 'mqtt test')
        payload = '{"Act":"test","msg":"%s"}' % msg
        mq.client.publish(topic, payload)
        ret = {'ret': '0', 'msg': '发送测试mqtt消息：topic=%s, payload=%s' % (topic, payload)}
        log.debug(ret['msg'])
        return HttpResponse(ret['msg'])
    except:
        log.error(traceback.format_exc())
        ret = {'ret': '0', 'msg': traceback.format_exc()}
        return HttpResponse(ret['msg'])


def refresh_access_token(request):

    referrer = request.META['HTTP_REFERER']
    appid = request.GET.get('appid', '')
    if appid:
        rs = get_access_token(appid=appid)
        return HttpResponseRedirect(referrer)
    else:
        raise Http404()


@csrf_exempt
def oauth2(request, menu_id):
    # 微信服务器回调接口
    # 需要 通过oauth 获得openid 的菜单
    menus = {
        "11": "/car/bind/",
        "12": "/car/location/",
        "13": "/car/status/",
        "21": "/car/owner/",
        "31": "http://weixin.hzweiyun.com/app/index.php?i=2&c=entry&mid=837&do=shop&m=ewei_shop",
    }
    code = request.GET.get("code")
    auth_token_url = 'https://api.weixin.qq.com/sns/oauth2/access_token?' \
                     'appid=wx4806b076e959f03f' \
                     '&secret=e5a474c94836f251f19ffee86613b1ab' \
                     '&code=%s' \
                     '&grant_type=authorization_code' % code
    # 获取 openid
    r = urllib2.urlopen(auth_token_url)
    rs = r.read()
    log.debug(rs)
    jr = json.loads(rs)

    # 带上openid访问本服务器的最终链接
    url = '%s?openid=%s' % (menus[menu_id], jr['openid'])

    return HttpResponseRedirect(url)


@csrf_exempt
def weixin_main(request):
    """
    所有的消息都会先进入这个函数进行处理，函数包含两个功能，
    微信接入验证是GET方法，
    微信正常的收发消息是用POST方法。
    """
    if request.method == "GET":
        # 从 request 中提取基本信息 (signature, timestamp, nonce, xml)
        signature = request.GET.get("signature", None)
        timestamp = request.GET.get("timestamp", None)
        nonce = request.GET.get("nonce", None)
        echostr = request.GET.get("echostr", None)

        #同时管理多个账户暂不进行验证
        # if not wechat.check_signature(
        #         signature=signature, timestamp=timestamp, nonce=nonce):
        #     return HttpResponseBadRequest('Verify Failed')

        return HttpResponse(echostr, content_type="text/plain")

    # POST 响应
    log.debug('收到：\n %s' % request.body)

    # 对 XML 数据进行解析 (必要, 否则不可执行 response_text, response_image 等操作)
    wechat.parse_data(request.body)
    # wechat.parse_data(body)
    # 获得解析结果, message 为 WechatMessage 对象 (wechat_sdk.messages中定义)
    message = wechat.get_message()
    # log.debug(message)
    mj = to_dict(message)
    del mj['raw']
    log.debug(mj)

    if message.target == 'gh_b99bc5badf13' or message.source == 'gh_b99bc5badf13':
        log.debug('公众号：康松车康王')
        return deal_with_cvx(message)
    elif message.target == 'gh_cfa401c66649' or message.source == 'gh_cfa401c66649':
        log.debug('测试号')
        return deal_with_cvx(message)

@csrf_exempt
def get_qrcode(request, appid, str):
    if len(appid) == 18 and is_valid(appid):
        pic_url = getQRCode(appid, str)
    else:
        pic_url = "不存在的 appid: %s" % appid
    return HttpResponse(pic_url)


def is_valid(appid):
    try:
        wa = WechatAccount.objects.get(appid=appid)
        return True
    except:
        return False


# 正则匹配去掉省市区
def get_clean_string(text):
    # text = "广东省深圳市宝安区广东省深圳市宝安区龙华新区清湖工业园清祥路1号宝能科技园9栋A座10"

    rules = [
        r'(.*?)省(.*)市(.*?)[区县](.*)',
        r'(.*)市(.*?)[区县](.*)',
    ]
    for regex in rules:
        matchObj = re.search(regex, text, re.M|re.I)

        if matchObj:
            for i in range(matchObj.lastindex):
                log.debug(matchObj.group(i))
            # print "matchObj.group() : ", matchObj.group()
            # print "matchObj.group(1) : ", matchObj.group(1)
            # print "matchObj.group(2) : ", matchObj.group(2)
            return matchObj.group(matchObj.lastindex)
        else:
            log.warning("No match!![%s]" % text)
    return text


# 解析出导航地址
def get_navigate_dst(text):
    """
    用正则匹配的方式进行简单的导航语义解析
    比如:
        我要 到 世界之窗
        我想 去 世界之窗
        到 世界之窗 去
        导航 到 世界之窗
    最后提取到真正的地址 [世界之窗]
    """
    rules = [
        r'(.*)到(.*)去',
        r'(.*)去(.*)',
        r'(.*)到(.*)',

        # 增加模式语音识别兼容
        # [导航 到 xxx] 可能会被识别成 [导航 的 xxx]
        r'(.*)的(.*)去',
        r'(.*)的(.*)',
    ]
    for regex in rules:
        matchObj = re.search(regex, text, re.M | re.I)

        if matchObj:
            # for i in range(matchObj.lastindex):
            #     log.debug(matchObj.group(i))
            log.debug('匹配到: %s' % regex)
            dst = matchObj.group(matchObj.lastindex)
            log.debug("解析到的目标地址:%s" % dst)
            return dst.replace('。', '')
    log.warning("No match!![%s]" % text)
    return ''


# 通过微信上报的经纬度确定用户的位置
def get_address(lat=39.983424, lng=116.322987):
    try:
        url = 'http://api.map.baidu.com/geocoder/v2/'
        data = {
            'output': 'json',
            'location': '39.983424,116.322987',
            'coordtype': 'gcj02ll',
            'ak': 'xabAK2yF5G57GVOykz0BQXLy'
        }
        rs = requests.get(url, params=data)
        return rs.json()['results']['addressComponent']
    except:
        log.warning('通过经纬度转换位置失败!')
        return None

#####
##### 处理具体某个公众号的信息
#####
def deal_with_cvx(message):
    response = ''
    try:
        if isinstance(message, TextMessage):
            log.debug('TextMessage：%s' % message.content)
            reply = text_cmd(message)
            # 机器人回复
            # if not reply:
            #     reply = get_robot_reply(message.content, message.source)
            if reply:
                response = wechat.response_text(content=reply)
        elif message.type == 'click':
            if message.key == '14':
                response = wechat.response_text(content='暂无故障')
        elif isinstance(message, EventMessage):  # 事件信息
            if message.type == 'subscribe':  # 关注事件(包括普通关注事件和扫描二维码造成的关注事件)
                if message.key and message.ticket:  # 如果 key 和 ticket 均不为空，则是扫描二维码造成的关注事件
                    log.debug('扫描带参数二维码关注')
                    arg = message.key[8:]
                    url = 'http://gards.covixon.com/car/bind?openid=%s&imei=%s' % (message.source, arg)
                    text = '您正在关联车辆:\r\n%s\r\n\r\n<a href="%s">点击输入授权码->></a>' % (arg, url)
                    log.debug('回复内容\n: %s' % text)
                    response = wechat.response_text(content=text)
                else:
                    try:
                        u = UserProfile.objects.get(openid=message.source)
                    except:
                        u = UserProfile()
                        u.openid = message.source
                        u.save()
                    response = wechat.response_text(content='欢迎关注康松车康王')
            elif message.type == 'scan':
                log.debug('关注后的扫描事件！')
                # 扫描事件
                if message.key:
                    url = 'http://gards.covixon.com/car/bind?openid=%s&imei=%s' % (message.source, message.key)
                    text = '您正在关联车辆:\r\n%s\r\n\r\n<a href="%s">点击输入授权码->></a>' % (message.key, url)
                    log.debug('回复内容:\n %s' % text)
                    response = wechat.response_text(content=text)
            elif message.type == 'location':
                log.debug('微信用户地理位置上报事件')
                lat = message.latitude
                lng = message.longitude
                addr = get_address(lat, lng)
                u = UserProfile.objects.get(openid=message.source)
                u.last_addr = json.dumps(addr)
                u.last_time = datetime.datetime.now()
                u.save()

        elif isinstance(message, LocationMessage):  # 位置信息
            if message.type == 'location':
                log.debug('选取发送地理位置')
                # text = {"location": message.location, "label": message.label, "scale": message.scale}
                name_text = message.label
                index = name_text.find('(')
                if index != -1:
                    name_text = name_text[:index]
                name_text = get_clean_string(name_text)
                payload = {
                           "Act": "Navi",
                           "Lat": str(message.location[0]),
                           "Lon": str(message.location[1]),
                           "Coord": "GCJ02",
                           "XCode": "",
                           "Name": name_text
                }
                pstr = to_echo(payload)
                log.debug('回复内容:\n %s' % pstr)
                mq.publish(get_imei(message.source), pstr, 2)

                msg = "来自微信目的地:\r\n [%s] \r\n已发送！\n(车辆所处位置可能会导致网络接收延迟)" % payload['Name']
                response = wechat.response_text(content=msg)
        elif isinstance(message, VoiceMessage):
            # 接口要先开启语音识别
            # voice_text = message.recognition
            msg = message.recognition.replace('。', '')
            dst = get_navigate_dst(message.recognition)
            if dst:
                url = 'http://api.map.baidu.com/place/v2/search'
                data = {
                    'output': 'json',
                    'q': dst,
                    'region': '中国',
                    'ret_coordtype': 'gcj02ll',
                    'ak': 'xabAK2yF5G57GVOykz0BQXLy'
                }
                rs = requests.get(url, params=data)
                ret = rs.json()['results']
                if len(ret) > 0:
                    location = rs.json()['results'][0]['location']

                    payload = {
                        "Act": "Navi",
                        "Lat": str(location['lat']),
                        "Lon": str(location['lng']),
                        "Coord": "GCJ02",
                        "XCode": "",
                        "Name": dst
                    }
                    pstr = to_echo(payload)
                    mq.publish(get_imei(message.source), pstr, 2)
                    msg = "识别到导航目的地:\n [%s] \n已解析发送到车机" % dst
                    log.debug(msg)
                else:
                    msg = "识别到导航目的地:\n [%s] \n 但地球上找不到这个位置,\n难道在火星上?\n请说个我知道的地方" % dst
            else:
                message.content = msg
                reply = text_cmd(message)

                if not reply:
                    reply = '未知命令!'
                msg = '[{}]\n{}'.format(msg, reply)

            response = wechat.response_text(content=msg)
    except:
        log.error(traceback.format_exc())
    return HttpResponse(response, content_type="application/xml")


# 根据用户 openid 获取对应的车辆imei
def get_imei(openid):
    imei = ''
    us = UserProfile.objects.filter(openid=openid)
    if len(us) > 0:
        u = us[0]
        if u.vehicle:
            imei = u.vehicle.imei
    return imei


def text_cmd(message):
    msg = ''
    text = message.content
    try:
        patterns = [
            # 各种模式的 固定指令 集合
            # 格式：
            #   (['文字指令1', '文字指令2'], '发往下位机的指令', '发完 回复给用户微信的信息',是否允许为关联用户下发到默认车辆False/True),
            # 下发指令为空时，不发送指令 直接回复微信消息。

            (['openid'], '', message.source),
            (['真人语音模式'], '{"Act":"UpAppProfile", "AUDIO": "WAV"}', '指令已发送',False),
            (['人工智能语音模式'], '{"Act":"UpAppProfile", "AUDIO": "TTS"}', '指令已发送',False),

            (['英文模式'], '{"Act":"UpAppProfile", "LANG": "USA"}', '指令已发送',False),
            (['中文模式'], '{"Act":"UpAppProfile", "LANG": "CHN", "DIALECT": "0"}', '指令已发送',False),
            (['淮北话模式', '淮普模式'], '{"Act":"UpAppProfile", "LANG": "CHN", "DIALECT": "0561"}', '指令已发送',False),
            (['常德话模式'], '{"Act":"UpAppProfile", "LANG": "CHN", "DIALECT": "0736"}', '指令已发送',False),

            (['新手模式'], '{"Act":"UpAppProfile", "MODE": "NEWBIE"}', '指令已发送',False),
            (['专家模式'], '{"Act":"UpAppProfile", "MODE": "EXPERT"}', '指令已发送',False),

            (['展示结束'], '{"Act":"PSaving4Exhibit", "Exhibit": "0"}', '指令已发送',False),
            (['展示开始'], '{"Act":"PSaving4Exhibit", "Exhibit": "1"}', '指令已发送',False),

            (['震动报警普通模式'], '{"Act":"GSAccuracy", "Accuracy":"5"}', '指令已发送',False),
            (['震动报警雷雨模式'], '{"Act":"GSAccuracy", "Accuracy":"9"}', '指令已发送',False),

            (['GS灵敏度极低'], '{"Act":"GSAccuracy", "Accuracy":"9"}', '指令已发送',False),
            (['GS灵敏度更低'], '{"Act":"GSAccuracy", "Accuracy":"8"}', '指令已发送',False),
            (['GS灵敏度较低'], '{"Act":"GSAccuracy", "Accuracy":"7"}', '指令已发送',False),
            (['GS灵敏度低'], '{"Act":"GSAccuracy", "Accuracy":"6"}', '指令已发送',False),
            (['GS灵敏度中'], '{"Act":"GSAccuracy", "Accuracy":"5"}', '指令已发送',False),
            (['GS灵敏度高'], '{"Act":"GSAccuracy", "Accuracy":"4"}', '指令已发送',False),
            (['GS灵敏度较高'], '{"Act":"GSAccuracy", "Accuracy":"3"}', '指令已发送',False),
            (['GS灵敏度更高'], '{"Act":"GSAccuracy", "Accuracy":"2"}', '指令已发送',False),
            (['GS灵敏度极高'], '{"Act":"GSAccuracy", "Accuracy":"1"}', '指令已发送',False),

            (['禁用休眠'], '{"Act":"SleepDisable"}', '指令已发送',False),
            (['启用休眠'], '{"Act":"SleepEnable"}', '指令已发送',False),

            (['休眠周期15'], '{"Act":"SleepPeriod", "Period":"15"}', '指令已发送',False),
            (['休眠周期20'], '{"Act":"SleepPeriod", "Period":"20"}', '指令已发送',False),
            (['休眠周期25'], '{"Act":"SleepPeriod", "Period":"25"}', '指令已发送',False),
            (['休眠周期30'], '{"Act":"SleepPeriod", "Period":"30"}', '指令已发送',False),

            (['节能保护模式'], '{"Act":"PSaving"}', '指令已发送',False),
            (['假日节能模式'], '{"Act":"PSaving"}', '指令已发送',False),

            (['点火开机模式'], '{"Act":"PNDPowerOnIgn", "OnIgn": "1"}', '指令已发送',False),
            (['普通开机模式'], '{"Act":"PNDPowerOnIgn", "OnIgn": "0"}', '指令已发送',False),

            (['测试VIN码读取'], '{"Act":"OBD_VIN_READ"}', '指令已发送',False),
            (['测试诊断请求'], '{"Act":"OBD_DTC_READ"}', '指令已发送',False),
            (['测试诊断清除'], '{"Act":"OBD_DTC_ERASE"}', '指令已发送',False),
            (['测试被动复位'], '{"Act":"BeReset"}', '指令已发送',False),

            (['上位机复位'], '{"Act":"UpAppReset"}', '指令已发送',False),

            (['后视镜关机'], '{"Act":"UpAppReset","POFF":"1"}', '指令已发送',False),
            (['后视镜开机'], '{"Act":"UpAppReset","PON":"1"}', '指令已发送',False),

            (['后视镜调试开启'], '{"Act":"UpAppLogcat","Log":"On"}', '指令已发送',False),
            (['后视镜调试关闭'], '{"Act":"UpAppLogcat","Log":"Off"}', '指令已发送',False),

            (['后视镜窗口显示'], '{"Act":"UpAppProfile","WND_DBG":"1"}', '指令已发送',False),
            (['后视镜窗口隐藏'], '{"Act":"UpAppProfile","WND_DBG":"0"}', '指令已发送',False),

            (['车康王调试窗口显示'], '{"Act":"UpAppProfile","WND_DBG":"1"}', '指令已发送',False),
            (['车康王调试窗口隐藏'], '{"Act":"UpAppProfile","WND_DBG":"0"}', '指令已发送',False),

            (['车康王车况窗口显示'], '{"Act":"UpAppProfile","WND_Vehicle":"1"}', '指令已发送',False),
            (['车康王车况窗口隐藏'], '{"Act":"UpAppProfile","WND_Vehicle":"0"}', '指令已发送',False),

            (['测试后视镜内存'], '{"Act":"UpAppMemDump"}', '指令已发送',False),

            (['切换道道通导航'], '{"Act":"UpAppProfile", "NAVIHOME":"RtNavi","NAVINAME":"RtNavi.exe","NAVITITLE":"RtNavi"}', '指令已发送',False),

            (['测试发送导航'], '{"Act":"Navi","Lat":"131.000000","Lon":"121.000000","Coord":"WGS84","XCode":"","Name":"shanghai"}', '指令已发送',False),

            (['测试计算日出日落'], '{"Act":"GetSunRiset"}', '指令已发送',False),

            (['测试主程序正式版升级'], '{"Act":"Upd","Path":"211.154.146.70:21,obd,obd888,/m/,mainV7d0c8e46R","MD5":"d76179ae10a18a86a06a6da3f86c809f","When":"2"}', '指令已发送',False),
            (['测试主程序正式版回滚'], '{"Act":"Upd","Path":"211.154.146.70:21,obd,obd888,/m/,mainVe9baffd7R","MD5":"9b2ceb4f002f1d0c37a69e4215a3e2fe","When":"2"}', '指令已发送',False),
            (['测试主程序调试版升级'], '{"Act":"Upd","Path":"211.154.146.70:21,obd,obd888,/m/,mainV7d0c8e46D","MD5":"873ea48189f451313569a191b58ccd39","When":"2"}', '指令已发送',False),
            (['测试主程序调试版回滚'], '{"Act":"Upd","Path":"211.154.146.70:21,obd,obd888,/m/,mainV0acceebdD","MD5":"cfeb53f712bc41f909353cd83cecb2bf","When":"2"}', '指令已发送',False),

            (['测试车匣子升级'], '{"Act":"Upd","Path":"211.154.146.70:21,obd,obd888,/v/,vcan201607292031","MD5":"3bd9af23bb25aa0cb17f65dceb094a2e","When":"2"}', '指令已发送',False),
            (['测试车匣子升级宝马'], '{"Act":"Upd","Path":"211.154.146.70:21,obd,obd888,/v/,vcanbmw20160730","MD5":"a7384c3157997d4448d85b2ffcfe3bf0","When":"2"}', '指令已发送',False),
            (['测试车匣子回滚'], '{"Act":"Upd","Path":"211.154.146.70:21,obd,obd888,/v/,vcan201607292030","MD5":"3079121a3c65a89703607e30fdf0b3d0","When":"2"}', '指令已发送',False),

            (['测试上位机升级'], '{"Act":"Upd","Path":"211.154.146.70:21,obd,obd888,/m/,main1b79e600","MD5":"d026eaa5bca32197c10407966310dba6","When":"2"}', '指令已发送',False),
            (['测试上位机升级'], '{"Act":"Upd","Path":"211.154.146.70:21,obd,obd888,/m/,main1b79e600","MD5":"d026eaa5bca32197c10407966310dba6","When":"2"}', '指令已发送',False),

            (['测试休眠三分钟'], '{"Act":"SleepPeriod","Period":"3"}', '指令已发送',False),
            (['测试休眠五分钟'], '{"Act":"SleepPeriod","Period":"5"}', '指令已发送',False),
            (['测试休眠十分钟'], '{"Act":"SleepPeriod","Period":"10"}', '指令已发送',False),
            (['测试休眠一刻钟'], '{"Act":"SleepPeriod","Period":"15"}', '指令已发送',False),
            (['测试休眠二刻钟'], '{"Act":"SleepPeriod","Period":"30"}', '指令已发送',False),
            (['测试休眠三刻钟'], '{"Act":"SleepPeriod","Period":"45"}', '指令已发送',False),
            (['测试休眠四刻钟'], '{"Act":"SleepPeriod","Period":"45"}', '指令已发送',False),
            (['测试休眠默认值'], '{"Act":"SleepPeriod","Period":"20"}', '指令已发送',False),

            (['测试省电电压开'], '{"Act":"VoltIfSleep","Volt":"12300"}', '指令已发送',False),
            (['测试省电电压关'], '{"Act":"VoltIfSleep","Volt":"10600"}', '指令已发送',False),

            (['测试报警电压20'], '{"Act":"VoltIfAlarm","Volt":"12000"}', '指令已发送',False),
            (['测试报警电压21'], '{"Act":"VoltIfAlarm","Volt":"12100"}', '指令已发送',False),
            (['测试报警电压22'], '{"Act":"VoltIfAlarm","Volt":"12200"}', '指令已发送',False),
            (['测试报警电压25'], '{"Act":"VoltIfAlarm","Volt":"12500"}', '指令已发送',False),
            (['测试报警电压27'], '{"Act":"VoltIfAlarm","Volt":"12700"}', '指令已发送',False),
            (['测试报警电压28'], '{"Act":"VoltIfAlarm","Volt":"12800"}', '指令已发送',False),
            (['测试报警电压29'], '{"Act":"VoltIfAlarm","Volt":"12900"}', '指令已发送',False),

            (['测试安全报警开'], '{"Act":"HzdButSec","alarm_if_hzd":"1"}', '指令已发送',False),
            (['测试安全报警关'], '{"Act":"HzdButSec","alarm_if_hzd":"0"}', '指令已发送',False),

            (['打开车门报警'], '{"Act":"Trespass","alarm_if_door":"1"}', '指令已发送',False),
            (['关闭车门报警'], '{"Act":"Trespass","alarm_if_door":"0"}', '指令已发送',False),
            (['打开非法开门报警'], '{"Act":"Trespass","alarm_if_door":"1"}', '指令已发送',False),
            (['关闭非法开门报警'], '{"Act":"Trespass","alarm_if_door":"0"}', '指令已发送',False),
            (['打开应急灯报警'], '{"Act":"Trespass","alarm_if_hzd":"1"}', '指令已发送',False),
            (['关闭应急灯报警'], '{"Act":"Trespass","alarm_if_hzd":"0"}', '指令已发送',False),

            (['这次借你下不为例'], '{"Act":"NOBorrow","Interval":"1","Times":"2"}', '指令已发送\r\n[拒绝借车功能将会播报2次发动机可能有严重故障]\r\n',False),
            (['老婆和车概不外借'], '{"Act":"NOBorrow","Interval":"1","Times":"3"}', '指令已发送\r\n[拒绝借车功能将会播报3次发动机可能有严重故障]\r\n',False),
            (['老婆与车概不外借'], '{"Act":"NOBorrow","Interval":"1","Times":"3"}', '指令已发送\r\n[拒绝借车功能将会播报3次发动机可能有严重故障]\r\n',False),
            (['老婆说车不外借'], '{"Act":"NOBorrow","Interval":"1","Times":"5"}', '指令已发送\r\n[拒绝借车功能将会播报5次发动机可能有严重故障]\r\n',False),
            (['借了看你好意思开'], '{"Act":"NOBorrow","Interval":"1","Times":"8"}', '指令已发送\r\n[拒绝借车功能将会播报8次发动机可能有严重故障]\r\n',False),
            (['都告诉你了车要修'], '{"Act":"NOBorrow","Interval":"1","Times":"15"}', '指令已发送\r\n[拒绝借车功能将会播报15次发动机可能有严重故障]\r\n',False),
            (['借车已归还'], '{"Act":"NOBorrow"}', '指令已发送,\r\n[拒绝借车功能已终止]\r\n',False),

            (['设置车型大众'], '{"Act":"SetVType", "Param":"WV2100"}', '指令已发送',False),
            (['设置车型大众MQB'], '{"Act":"SetVType", "Param":"WA2106"}', '指令已发送',False),
            (['设置车型大众MBQ'], '{"Act":"SetVType", "Param":"WA2106"}', '指令已发送',False),
            (['设置车型大众朗逸'], '{"Act":"SetVType", "Param":"WV2103"}', '指令已发送',False),

            (['设置车型斯柯达'], '{"Act":"SetVType", "Param":"WV2100"}', '指令已发送',False),
            (['设置车型斯柯达昕锐'], '{"Act":"SetVType", "Param":"WV2100"}', '指令已发送',False),
            (['设置车型斯柯达明锐'], '{"Act":"SetVType", "Param":"WA2106"}', '指令已发送',False),

            (['设置车型奥迪'], '{"Act":"SetVType", "Param":"WA2101"}', '指令已发送',False),
            (['设置车型奥迪A3'], '{"Act":"SetVType", "Param":"WA2104"}', '指令已发送',False),
            (['设置车型奥迪A6'], '{"Act":"SetVType", "Param":"WA2105"}', '指令已发送',False),
            (['设置车型奥迪A6L08'], '{"Act":"SetVType", "Param":"WA2102"}', '指令已发送',False),
            (['设置车型奥迪Q3'], '{"Act":"SetVType", "Param":"WA2104"}', '指令已发送',False),
            (['设置车型奥迪Q5'], '{"Act":"SetVType", "Param":"WA2101"}', '指令已发送',False),
            (['设置车型奥迪Q7'], '{"Act":"SetVType", "Param":"WA2102"}', '指令已发送',False),

            (['设置车型丰田'], '{"Act":"SetVType", "Param":"JT2100"}', '指令已发送',False),
            (['设置车型丰田凯美瑞'], '{"Act":"SetVType", "Param":"JT2101"}', '指令已发送',False),

            (['设置车型日产'], '{"Act":"SetVType", "Param":"JN2100"}', '指令已发送',False),
            (['设置车型日产天籁'], '{"Act":"SetVType", "Param":"JN2101"}', '指令已发送',False),
            (['设置车型日产贵士'], '{"Act":"SetVType", "Param":"JN2102"}', '指令已发送',False),
            (['设置车型日产奇骏'], '{"Act":"SetVType", "Param":"JN2103"}', '指令已发送',False),
            (['设置车型日产启辰'], '{"Act":"SetVType", "Param":"JN2104"}', '指令已发送',False),

            (['设置车型本田'], '{"Act":"SetVType", "Param":"JH2100"}', '指令已发送',False),
            (['设置车型本田风范'], '{"Act":"SetVType", "Param":"JH2101"}', '指令已发送',False),

            (['设置车型马自达'], '{"Act":"SetVType", "Param":"J62100"}', '指令已发送',False),
            (['设置车型马自达CX5'], '{"Act":"SetVType", "Param":"J62100"}', '指令已发送',False),

            (['设置车型宝马'], '{"Act":"SetVType", "Param":"WB2100"}', '指令已发送',False),
            (['设置车型宝马100K'], '{"Act":"SetVType", "Param":"WB2101"}', '指令已发送',False),
            (['设置车型宝马125K'], '{"Act":"SetVType", "Param":"WB2102"}', '指令已发送',False),
            (['设置车型宝马3'], '{"Act":"SetVType", "Param":"WB2101"}', '指令已发送',False),
            (['设置车型宝马5'], '{"Act":"SetVType", "Param":"WB2101"}', '指令已发送',False),

            (['设置车型奔驰'], '{"Act":"SetVType", "Param":"WD2100"}', '指令已发送',False),

            (['设置车型通用'], '{"Act":"SetVType", "Param":"1G2100"}', '指令已发送',False),
            (['设置车型通用别克'], '{"Act":"SetVType", "Param":"1G2100"}', '指令已发送',False),
            (['设置车型通用雪佛兰'], '{"Act":"SetVType", "Param":"1G2100"}', '指令已发送',False),
            (['设置车型通用凯迪拉克'], '{"Act":"SetVType", "Param":"1G2100"}', '指令已发送',False),

            (['设置车型福特'], '{"Act":"SetVType", "Param":"1F2100"}', '指令已发送',False),
            (['设置车型福特新福克斯'], '{"Act":"SetVType", "Param":"1F2100"}', '指令已发送',False),
            (['设置车型福特经典福克斯'], '{"Act":"SetVType", "Param":"1F2101"}', '指令已发送',False),
            (['设置车型福特新蒙迪欧'], '{"Act":"SetVType", "Param":"1F2102"}', '指令已发送',False),
            (['设置车型福特锐界'], '{"Act":"SetVType", "Param":"1F2103"}', '指令已发送',False),
            (['设置车型福特嘉年华'], '{"Act":"SetVType", "Param":"1F2104"}', '指令已发送',False),
            (['设置车型福特翼博'], '{"Act":"SetVType", "Param":"1F2104"}', '指令已发送',False),
            (['设置车型福特ECP'], '{"Act":"SetVType", "Param":"1F2105"}', '指令已发送',False),

            (['设置车型现代'], '{"Act":"SetVType", "Param":"KM2100"}', '指令已发送',False),
            (['设置车型起亚'], '{"Act":"SetVType", "Param":"KM2100"}', '指令已发送',False),

            (['设置车型标志'], '{"Act":"SetVType", "Param":"VP2100"}', '指令已发送',False),
            (['设置车型雪铁龙'], '{"Act":"SetVType", "Param":"VP2100"}', '指令已发送',False),

            (['测试起步提醒'], '{"Act":"CAN_EMU_4_IDH", "Emu":"Start"}', '指令已发送',False),
            (['测试倒车提醒'], '{"Act":"CAN_EMU_4_IDH", "Emu":"Reverse"}', '指令已发送',False),
            (['测试离车提醒'], '{"Act":"CAN_EMU_4_IDH", "Emu":"Goodbye"}', '指令已发送',False),

            (['测试钥匙遗忘'], '{"Act":"CAN_EMU_4_IDH", "Emu":"KeyLeave"}', '指令已发送',False),

            (['测试重复点火'], '{"Act":"CAN_EMU_4_IDH", "Emu":"IgnRetry"}', '指令已发送',False),
            (['测试点火失败'], '{"Act":"CAN_EMU_4_IDH", "Emu":"IgnFail"}', '指令已发送',False),
            (['测试怠速过久'], '{"Act":"CAN_EMU_4_IDH", "Emu":"LongIdle"}', '指令已发送',False),
            (['测试疲劳驾驶'], '{"Act":"CAN_EMU_4_IDH", "Emu":"LongDrive"}', '指令已发送',False),

            (['测试晚间开灯'], '{"Act":"CAN_EMU_4_IDH", "Emu":"LBeam2On"}', '指令已发送',False),
            (['测试关闭远光'], '{"Act":"CAN_EMU_4_IDH", "Emu":"HBeam2Off"}', '指令已发送',False),

            (['测试临时停车'], '{"Act":"CAN_EMU_4_IDH", "Emu":"TmpStop"}', '指令已发送',False),
            (['测试空挡滑行'], '{"Act":"CAN_EMU_4_IDH", "Emu":"Taxiing"}', '指令已发送',False),

            (['测试手刹未放'], '{"Act":"CAN_EMU_4_IDH", "Emu":"Parking2Put"}', '指令已发送',False),

            (['测试副驾车门'], '{"Act":"CAN_EMU_4_IDH", "Emu":"DoorRFAjar"}', '指令已发送',False),
            (['测试右后车门'], '{"Act":"CAN_EMU_4_IDH", "Emu":"DoorRRAjar"}', '指令已发送',False),

            (['测试副驾窗开'], '{"Act":"CAN_EMU_4_IDH", "Emu":"WndRFOpen"}', '指令已发送',False),
            (['测试副驾窗关'], '{"Act":"CAN_EMU_4_IDH", "Emu":"WndRFClose"}', '指令已发送',False),

            (['测试安全带开'], '{"Act":"CAN_EMU_4_IDH", "Emu":"BeltLFOut"}', '指令已发送',False),

            (['测试转速三千'], '{"Act":"CAN_EMU_4_IDH", "Emu":"RPM3K"}', '指令已发送',False),
            (['测试转速四千'], '{"Act":"CAN_EMU_4_IDH", "Emu":"RPM4K"}', '指令已发送',False),

            (['测试车速三十'], '{"Act":"CAN_EMU_4_IDH", "Emu":"KM030"}', '指令已发送',False),
            (['测试车速四十'], '{"Act":"CAN_EMU_4_IDH", "Emu":"KM040"}', '指令已发送',False),

            (['左舵车'], '{"Act":"LHT"}', '指令已发送',False),
            (['右舵车'], '{"Act":"RHT"}', '指令已发送',False),
            (['左侧驾驶'], '{"Act":"LHT"}', '指令已发送',False),
            (['右侧驾驶'], '{"Act":"RHT"}', '指令已发送',False),

            (['自动挡'], '{"Act":"MT", "MT":"0"}', '指令已发送',False),
            (['手动挡'], '{"Act":"MT", "MT":"1"}', '指令已发送',False),

            (['设置变速箱自动'], '{"Act":"MT", "MT":"0"}', '指令已发送',False),
            (['设置变速箱手动'], '{"Act":"MT", "MT":"1"}', '指令已发送',False),

            (['实习模式'], '{"Act":"DriveLevel", "level": "newbie"}', '指令已发送',False),
            (['老司机模式'], '{"Act":"DriveLevel", "level": "skilled"}', '指令已发送',False),

            (['关闭车窗提醒'], '{"Act":"PromptWnd", "prompt": "0"}', '指令已发送',False),
            (['开启车窗提醒'], '{"Act":"PromptWnd", "prompt": "1"}', '指令已发送',False),
            (['关闭车速提醒'], '{"Act":"PromptVSPD", "prompt": "0"}', '指令已发送',False),
            (['开启车速提醒'], '{"Act":"PromptVSPD", "prompt": "1"}', '指令已发送',False),
            (['关闭转速提醒'], '{"Act":"PromptRPM", "prompt": "0"}', '指令已发送',False),
            (['开启转速提醒'], '{"Act":"PromptRPM", "prompt": "1"}', '指令已发送',False),

            (['手刹暂不支持'], '{"Act":"NonSupportParking"}', '指令已发送',False),
            (['手刹支持'], '{"Act":"AddSupportParking"}', '指令已发送',False),

            (['安全带暂不支持'], '{"Act":"NonSupportBelt"}', '指令已发送',False),
            (['安全带支持'], '{"Act":"AddSupportBelt"}', '指令已发送',False),

            (['主驾安全带暂不支持'], '{"Act":"NonSupportBeltLF"}', '指令已发送',False),
            (['主驾安全带支持'], '{"Act":"AddSupportBeltLF"}', '指令已发送',False),

            (['副驾安全带暂不支持'], '{"Act":"NonSupportBeltRF"}', '指令已发送',False),
            (['副驾安全带支持'], '{"Act":"AddSupportBeltRF"}', '指令已发送',False),

            (['油量暂不支持'], '{"Act":"VSSupp","NonSuppFuelLevel":"1"}', '指令已发送',False),
            (['油量支持'], '{"Act":"VSSupp","NonSuppFuelLevel":"1"}', '指令已发送',False),
            (['冷却液温度暂不支持'], '{"Act":"VSSupp","NonSuppCoolant":"1"}', '指令已发送',False),
            (['冷却液温度支持'], '{"Act":"VSSupp","NonSuppCoolant":"1"}', '指令已发送',False),
            (['转速暂不支持'], '{"Act":"VSSupp","NonSuppRPM":"1"}', '指令已发送',False),
            (['转速支持'], '{"Act":"VSSupp","NonSuppRPM":"1"}', '指令已发送',False),
            (['车速暂不支持'], '{"Act":"VSSupp","NonSuppVSPD":"1"}', '指令已发送',False),
            (['车速支持'], '{"Act":"VSSupp","NonSuppVSPD":"1"}', '指令已发送',False),
            (['里程暂不支持'], '{"Act":"VSSupp","NonSuppODO":"1"}', '指令已发送',False),
            (['里程支持'], '{"Act":"VSSupp","NonSuppODO":"1"}', '指令已发送',False),

            (['模拟趴车档'], '{"Act":"CAN_EMU_4_IDH", "action": "gearP"}', '指令已发送',False),
            (['模拟倒车档'], '{"Act":"CAN_EMU_4_IDH", "action": "gearR"}', '指令已发送',False),
            (['模拟空档'], '{"Act":"CAN_EMU_4_IDH", "action": "gearN"}', '指令已发送',False),
            (['模拟前进档'], '{"Act":"CAN_EMU_4_IDH", "action": "gearD"}', '指令已发送',False),

            (['模拟主驾车门开'], '{"Act":"CAN_EMU_4_IDH", "action": "DoorLFOpen"}', '指令已发送',False),
            (['模拟主驾车门关'], '{"Act":"CAN_EMU_4_IDH", "action": "DoorLFClose"}', '指令已发送',False),
            (['模拟副驾车门开'], '{"Act":"CAN_EMU_4_IDH", "action": "DoorRFOpen"}', '指令已发送',False),
            (['模拟副驾车门关'], '{"Act":"CAN_EMU_4_IDH", "action": "DoorRFClose"}', '指令已发送',False),

            (['模拟主驾车窗开'], '{"Act":"CAN_EMU_4_IDH", "action": "wndLFOpen"}', '指令已发送',False),
            (['模拟主驾车窗关'], '{"Act":"CAN_EMU_4_IDH", "action": "wndLFClose"}', '指令已发送',False),
            (['模拟副驾车窗开'], '{"Act":"CAN_EMU_4_IDH", "action": "wndRFOpen"}', '指令已发送',False),
            (['模拟副驾车窗关'], '{"Act":"CAN_EMU_4_IDH", "action": "wndRFClose"}', '指令已发送',False),

            (['模拟手刹拉起'], '{"Act":"CAN_EMU_4_IDH", "action": "ParkingPull"}', '指令已发送',False),
            (['模拟手刹放下'], '{"Act":"CAN_EMU_4_IDH", "action": "ParkingPut"}', '指令已发送',False),

            (['模拟钥匙拔出'], '{"Act":"CAN_EMU_4_IDH", "action": "KeyOut"}', '指令已发送',False),
            (['模拟钥匙通电'], '{"Act":"CAN_EMU_4_IDH", "action": "KeyACC"}', '指令已发送',False),
            (['模拟钥匙点火'], '{"Act":"CAN_EMU_4_IDH", "action": "KeyStart"}', '指令已发送',False),

            (['模拟锁车'], '{"Act":"CAN_EMU_4_IDH", "action": "RKeyLock"}', '指令已发送',False),
            (['模拟解锁'], '{"Act":"CAN_EMU_4_IDH", "action": "RKeyUnlock"}', '指令已发送',False),

            (['拔钥匙提示离车'], '{"Act":"ByeOnKeyNA", "Bye": "1"}', '指令已发送',False),
            (['开门提示离车'], '{"Act":"ByeOnKeyNA", "Bye": "0"}', '指令已发送',False),
            
            (['休眠待机模式'], '{"Act":"ShutdownAsSleep", "Shutdown": "0"}', '指令已发送',False),
            (['关机待机模式'], '{"Act":"ShutdownAsSleep", "Shutdown": "1"}', '指令已发送',False),
            
            (['关闭车窗'], '{"Act":"WndUp","Wnd":"All"}', '指令已发送',False),
            (['关闭主驾车窗'], '{"Act":"WndUp","Wnd":"LF"}', '指令已发送',False),
            (['关闭副驾车窗'], '{"Act":"WndUp","Wnd":"RF"}', '指令已发送',False),
            (['关闭左后车窗'], '{"Act":"WndUp","Wnd":"LR"}', '指令已发送',False),
            (['关闭右后车窗'], '{"Act":"WndUp","Wnd":"RR"}', '指令已发送',False),
            (['打开车窗'], '{"Act":"WndDn","Wnd":"All"}', '指令已发送',False),
            (['打开主驾车窗'], '{"Act":"WndDn","Wnd":"LF"}', '指令已发送',False),
            (['打开副驾车窗'], '{"Act":"WndDn","Wnd":"RF"}', '指令已发送',False),
            (['打开左后车窗'], '{"Act":"WndDn","Wnd":"LR"}', '指令已发送',False),
            (['打开右后车窗'], '{"Act":"WndDn","Wnd":"RR"}', '指令已发送',False),
            
            (['系统复位'], '{"Category":"FactoryReset","Cmd":"Reset"}', '指令已发送',False),
            
            (['系统开关'], '{"Category":"System","Cmd":"Qry","On":"1"}', '指令已发送',False),
            (['系统开启'], '{"Category":"System","Cmd":"Set","On":"1"}', '指令已发送',False),
            (['系统关闭'], '{"Category":"System","Cmd":"Set","On":"0"}', '指令已发送',False),

            (['语音提醒开关'], '{"Category":"VoicePrompt","Cmd":"Qry","On":"1"}', '指令已发送',False),
            (['语音提醒开启'], '{"Category":"VoicePrompt","Cmd":"Set","On":"1"}', '指令已发送',False),
            (['语音提醒关闭'], '{"Category":"VoicePrompt","Cmd":"Set","On":"0"}', '指令已发送',False),

            (['疲劳灵敏度'], '{"Category":"FatigeLevel","Cmd":"Qry","Level":"0"}', '指令已发送',False),
            (['疲劳灵敏度高'], '{"Category":"FatigeLevel","Cmd":"Set","Level":"0"}', '指令已发送',False),
            (['疲劳灵敏度中'], '{"Category":"FatigeLevel","Cmd":"Set","Level":"1"}', '指令已发送',False),
            (['疲劳灵敏度低'], '{"Category":"FatigeLevel","Cmd":"Set","Level":"2"}', '指令已发送',False),

            (['超速提醒开关'], '{"Category":"OverSpeedPrompt","Cmd":"Qry","On":"1"}', '指令已发送',False),
            (['超速提醒开启'], '{"Category":"OverSpeedPrompt","Cmd":"Set","On":"1"}', '指令已发送',False),
            (['超速提醒关闭'], '{"Category":"OverSpeedPrompt","Cmd":"Set","On":"0"}', '指令已发送',False),

            (['疲劳驾驶状态开关'], '{"Category":"DrivingFatige","Cmd":"Qry","On":"1"}', '指令已发送',False),
            (['疲劳驾驶状态开启'], '{"Category":"DrivingFatige","Cmd":"Set","On":"1"}', '指令已发送',False),
            (['疲劳驾驶状态关闭'], '{"Category":"DrivingFatige","Cmd":"Set","On":"0"}', '指令已发送',False),

            (['超长驾驶状态开关'], '{"Category":"DrivingLong","Cmd":"Qry","On":"1"}', '指令已发送',False),
            (['超长驾驶状态开启'], '{"Category":"DrivingLong","Cmd":"Set","On":"1"}', '指令已发送',False),
            (['超长驾驶状态关闭'], '{"Category":"DrivingLong","Cmd":"Set","On":"0"}', '指令已发送',False),

            (['后视镜下翻开关'], '{"Category":"RVMFolding","Cmd":"Qry","On":"1"}', '指令已发送',False),
            (['后视镜下翻开启'], '{"Category":"RVMFolding","Cmd":"Set","On":"1"}', '指令已发送',False),
            (['后视镜下翻关闭'], '{"Category":"RVMFolding","Cmd":"Set","On":"0"}', '指令已发送',False),

            (['自动转向开关'], '{"Category":"AutoTurnSignal","Cmd":"Qry","On":"1"}', '指令已发送',False),
            (['自动转向开启'], '{"Category":"AutoTurnSignal","Cmd":"Set","On":"1"}', '指令已发送',False),
            (['自动转向关闭'], '{"Category":"AutoTurnSignal","Cmd":"Set","On":"0"}', '指令已发送',False),

            (['雨刮升窗开关'], '{"Category":"WndUpByWiper","Cmd":"Qry","On":"1"}', '指令已发送',False),
            (['雨刮升窗开启'], '{"Category":"WndUpByWiper","Cmd":"Set","On":"1"}', '指令已发送',False),
            (['雨刮升窗关闭'], '{"Category":"WndUpByWiper","Cmd":"Set","On":"0"}', '指令已发送',False),

            (['空调升窗开关'], '{"Category":"WndUpByAC","Cmd":"Qry","On":"1"}', '指令已发送',False),
            (['空调升窗开启'], '{"Category":"WndUpByAC","Cmd":"Set","On":"1"}', '指令已发送',False),
            (['空调升窗关闭'], '{"Category":"WndUpByAC","Cmd":"Set","On":"0"}', '指令已发送',False),

            (['静音开关'], '{"Category":"Mute","Cmd":"Set","Qry":"1"}', '指令已发送',False),
            (['静音打开'], '{"Category":"Mute","Cmd":"Set","On":"1"}', '指令已发送',False),
            (['静音关闭'], '{"Category":"Mute","Cmd":"Set","On":"0"}', '指令已发送',False),

            (['喇叭音量'], '{"Category":"SpeakerVolume","Cmd":"Qry","Level":"0"}', '指令已发送',False),
            (['喇叭音量高'], '{"Category":"SpeakerVolume","Cmd":"Set","Level":"0"}', '指令已发送',False),
            (['喇叭音量中'], '{"Category":"SpeakerVolume","Cmd":"Set","Level":"1"}', '指令已发送',False),
            (['喇叭音量低'], '{"Category":"SpeakerVolume","Cmd":"Set","Level":"2"}', '指令已发送',False),

            (['自动转向灵敏度'], '{"Category":"AutoTurnSignalLevel","Cmd":"Qry","Level":"0"}', '指令已发送',False),
            (['自动转向灵敏度高'], '{"Category":"AutoTurnSignalLevel","Cmd":"Set","Level":"0"}', '指令已发送',False),
            (['自动转向灵敏度中'], '{"Category":"AutoTurnSignalLevel","Cmd":"Set","Level":"1"}', '指令已发送',False),
            (['自动转向灵敏度低'], '{"Category":"AutoTurnSignalLevel","Cmd":"Set","Level":"2"}', '指令已发送',False),

            (['ONOFF语音播报开关'], '{"Category":"OnOffPrompt","Cmd":"Qry","On":"0"}', '指令已发送',False),
            (['ONOFF语音播报打开'], '{"Category":"OnOffPrompt","Cmd":"Set","On":"0"}', '指令已发送',False),
            (['ONOFF语音播报关闭'], '{"Category":"OnOffPrompt","Cmd":"Set","On":"255"}', '指令已发送',False),

            (['自动大灯灵敏度'], '{"Category":"AutoLowBeamLevel","Cmd":"Qry","Level":"0"}', '指令已发送',False),
            (['自动大灯灵敏度高'], '{"Category":"AutoLowBeamLevel","Cmd":"Set","Level":"0"}', '指令已发送',False),
            (['自动大灯灵敏度中'], '{"Category":"AutoLowBeamLevel","Cmd":"Set","Level":"1"}', '指令已发送',False),
            (['自动大灯灵敏度低'], '{"Category":"AutoLowBeamLevel","Cmd":"Set","Level":"2"}', '指令已发送',False),

            (['位移传感器检测开关'], '{"Category":"DispacementSensor","Cmd":"Qry","On":"255"}', '指令已发送',False),
            (['位移传感器检测开启'], '{"Category":"DispacementSensor","Cmd":"Set","On":"255"}', '指令已发送',False),
            (['位移传感器检测关闭'], '{"Category":"DispacementSensor","Cmd":"Set","On":"0"}', '指令已发送',False),

            (['车型查询'], '{"Category":"Model","Cmd":"Qry","Model":"255", "Platform":"PQ"}', '指令已发送',False),
            (['车型PQ1'], '{"Category":"Model","Cmd":"Set","Model":"255", "Platform":"PQ"}', '指令已发送',False),
            (['车型PQ2'], '{"Category":"Model","Cmd":"Set","Model":"0", "Platform":"PQ"}', '指令已发送',False),

            (['车型MQB1'], '{"Category":"Model","Cmd":"Set","Model":"255", "Platform":"MQB"}', '指令已发送',False),
            (['车型MQB2'], '{"Category":"Model","Cmd":"Set","Model":"85", "Platform":"MQB"}', '指令已发送',False),
            (['车型MQB3'], '{"Category":"Model","Cmd":"Set","Model":"0", "Platform":"MQB"}', '指令已发送',False),

            (['展示模式'], '{"Category":"ExhibitionMode","Cmd":"Qry","On":"0"}', '指令已发送',False),
            (['展示模式开启'], '{"Category":"ExhibitionMode","Cmd":"Set","On":"0"}', '指令已发送',False),
            (['展示模式关闭'], '{"Category":"ExhibitionMode","Cmd":"Set","On":"255"}', '指令已发送',False),
			
            (['关闭看门狗'], '{"Act":"WDOff","WD":"0"}', '指令已发送',False),
        ]

        for pattern in patterns:
            if text in pattern[0]:
                if pattern[1]:
                    imei = get_imei(message.source)
                    if imei:
                        mq.publish(imei, pattern[1])
                        msg = pattern[2]
                    elif pattern[3]:
                        # 没关联 但允许发指令
                        t = get_imei(openid='test')
                        mq.publish(t, pattern[1])
                        msg = pattern[2]
                    else:
                        # 没关联也不允许发指令
                        # 只 回复信息
                        msg = pattern[2]
                        # msg = '您没关联车辆'
                else:
                    msg = pattern[2]
                return msg

        # 文字 加锁 上锁 emoji上锁图标
        if text in ['锁车', '上锁', '\ue144']:
            msg = '您没关联车辆'
            us = UserProfile.objects.filter(openid=message.source)
            if len(us) > 0:
                u = us[0]
                if u.vehicle:
                    last = get_last(u.vehicle)
                    ID = ''
                    ts = str(int(time.time()))
                    if last and 'ID' in last:
                        ID = last['ID']['value']
                    payload = '{"Act":"Lock", "ID":"%s", "ts":"%s"}' % (ID, ts)
                    mq.publish(u.vehicle.imei, payload)
                    msg = "\ue144指令已发送\r\n[指令仅在停车时执行]\r\n[车辆所处位置可能会导致网络接收延迟,可连续发送以确保安全]"
            return msg

        if text in ['锁车长按', '上锁长按', '\ue144']:
            msg = '您没关联车辆'
            us = UserProfile.objects.filter(openid=message.source)
            if len(us) > 0:
                u = us[0]
                if u.vehicle:
                    last = get_last(u.vehicle)
                    ID = ''
                    ts = str(int(time.time()))
                    if last and 'ID' in last:
                        ID = last['ID']['value']
                    payload = '{"Act":"LockLP", "ID":"%s", "ts":"%s"}' % (ID, ts)
                    mq.publish(u.vehicle.imei, payload)
                    msg = "\ue144指令已发送\r\n[指令仅在停车时执行]\r\n[车辆所处位置可能会导致网络接收延迟,可连续发送以确保安全]"
            return msg

        if text in ['解锁', '开锁', '\ue145']:
            msg = '您没关联车辆'
            us = UserProfile.objects.filter(openid=message.source)
            if len(us) > 0:
                u = us[0]
                if u.vehicle:
                    last = get_last(u.vehicle)
                    ID = ''
                    ts = str(int(time.time()))
                    if last and 'ID' in last:
                        ID = last['ID']['value']
                    payload = '{"Act":"UNLock", "ID":"%s", "ts":"%s"}' % (ID, ts)
                    if u.vehicle.imei:
                        mq.publish(u.vehicle.imei, payload)
                        msg = "\ue145指令已发送\r\n[请仅在临时应急时使用]\r\n[请务必守在车旁执行]\r\n[指令仅在三分钟有效]\n[发送后三分钟内请勿离开]"
            return msg

        if text in ['解锁长按', '开锁长按', '\ue145']:
            msg = '您没关联车辆'
            us = UserProfile.objects.filter(openid=message.source)
            if len(us) > 0:
                u = us[0]
                if u.vehicle:
                    last = get_last(u.vehicle)
                    ID = ''
                    ts = str(int(time.time()))
                    if last and 'ID' in last:
                        ID = last['ID']['value']
                    payload = '{"Act":"UNLockLP", "ID":"%s", "ts":"%s"}' % (ID, ts)
                    if u.vehicle.imei:
                        mq.publish(u.vehicle.imei, payload)
                        msg = "\ue145指令已发送\r\n[请仅在临时应急时使用]\r\n[请务必守在车旁执行]\r\n[指令仅在三分钟有效]\n[发送后三分钟内请勿离开]"
            return msg

        if text.startswith('SetVType:'):
            vt = text.split(':')[1]
            msg = '您没关联车辆'
            us = UserProfile.objects.filter(openid=message.source)
            if len(us) > 0:
                u = us[0]
                if u.vehicle:
                    payload = '{"Act":"SetVType", "Param":"%s"}' % vt
                    if u.vehicle.imei:
                        mq.publish(u.vehicle.imei, payload)
                        msg = "指令已发送"
            return msg

        if text.startswith('VIN:'):
            vin = text.split(':')[1]
            msg = '您没关联车辆'
            us = UserProfile.objects.filter(openid=message.source)
            if len(us) > 0:
                u = us[0]
                if u.vehicle:
                    payload = '{"Act":"VIN_SET", "VIN":"%s"}' % vin
                    if u.vehicle.imei:
                        mq.publish(u.vehicle.imei, payload)
                        msg = "指令已发送"
            return msg

        # 高德地图 url
        amap_url = 'http://m.amap.com/detail/mapview/__'
        def urldecode(query):
            d = {}
            a = query.split('&')
            for s in a:
                if s.find('='):
                    k,v = map(urllib.unquote, s.split('='))
                    try:
                        d[k].append(v)
                    except KeyError:
                        d[k] = [v]

            return d

        if text.startswith(amap_url):
            oarg = text[amap_url.__len__():]
            do = urldecode(oarg)
            pl = do['p'][0].split(',')
            lat = pl[1]
            lon = pl[2]
            ostr = pl[3] + pl[4]
            label = str(ostr.encode('ISO-8859-1'))

            payload = {
                       "Act": "Navi",
                       "Lat": lat,
                       "Lon": lon,
                       "Coord": "GCJ02",
                       "XCode": "",
                       "Name": label
            }
            pstr = to_echo(payload)
            log.debug('回复内容:\n %s' % pstr)
            mq.publish(get_imei(message.source), pstr, 2)
            msg = "来自高德地图目的地:\r\n [%s] \r\n已发送！\n(车辆所处位置可能会导致网络接收延迟)" % payload['Name']
            return msg

        if text.startswith('注意:') or text.startswith('请注意:'):
            payload = {"Act": "PushMsg", "Occasion": "2", "Msg": text}
            pstr = to_echo(payload)
            mq.publish(get_imei(message.source), pstr, 2)
            msg = pstr
            return msg

        if text.__contains__('下车别忘了'):
            payload = {"Act": "PushMsg", "Occasion": "5", "Msg": text}
            pstr = to_echo(payload)
            mq.publish(get_imei(message.source), pstr, 2)
            msg = pstr
            return msg
            
        if text == 'mqtt_q0':
            payload = {"Act": "mqtt_publish", "qos": "0"}
            pstr = to_echo(payload)
            mq.publish('LM861358034217752', pstr, 0)
            msg = "mqtt_q0"
            return None
            
        if text == 'mqtt_q1':
            payload = {"Act": "mqtt_publish", "qos": "1"}
            pstr = to_echo(payload)
            mq.publish('LM861358034217752', pstr, 1)
            msg = "mqtt_q1"
            return None
            
        if text == 'mqtt_q2':
            payload = {"Act": "mqtt_publish", "qos": "2"}
            pstr = to_echo(payload)
            mq.publish('LM861358034217752', pstr, 2)
            msg = "mqtt_q2"
            return None
            
        if text == '切换':
            us = UserProfile.objects.filter(openid=message.source)
            if len(us) > 0:
                u = us[0]
                if not u.vehicle and (not u.vehicles or u.vehicles=='[]'):
                    msg = '你没有关联的车辆!'
                elif u.vehicle.imei in u.vehicles and not u.vehicles.__contains__(','):
                    msg = '关联了一辆[%s],并且已经设置了当前关联' % u.vehicle.imei
                else:
                    vcs = json.loads(u.vehicles)
                    msg = '请回复 s+序号切换(如切换到第二个,回复 s2):\n'
                    for i in range(len(vcs)):
                        msg += "[%s] %s\n" % (i + 1, vcs[i])
            return msg

        # 大小写不限
        if text.startswith('s') or text.startswith('S'):
            nu = int(text[1:])
            us = UserProfile.objects.filter(openid=message.source)
            if len(us) > 0:
                u = us[0]
                if not u.vehicle and (not u.vehicles or u.vehicles == '[]'):
                    msg = '你没有关联的车辆!'
                elif u.vehicle.imei in u.vehicles and not u.vehicles.__contains__(','):
                    msg = '关联了一辆[%s],并且已经设置了当前关联' % u.vehicle.imei
                else:
                    vcs = json.loads(u.vehicles)

                    for i in range(len(vcs)):
                        if i == nu-1:
                            dev = Device.objects.get(imei=vcs[i])
                            u.vehicle = dev
                            u.save()
                            return '已切换当前车辆为[%s]' % dev.imei
            return msg
        if text.startswith('dx') or text.startswith('DX'):
            dx = text.split(',')
            if len(dx) < 3:
                return '格式错误!\n发短信格式:\nDX,号码,短信内容'
            else:
                rs = quectel(dx[1], dx[2])
                if rs:
                    return '短信已发送'
                else:
                    return '短信无法发送,请检查号码是否正确'
        if text.startswith('{"Act":"'):
            mq.publish(get_imei(message.source), text, 1)
            msg = '已发送'
            return msg
    except:
        log.error(traceback.format_exc())
    return msg


def deal_with_test(message):
    response = ''
    try:
        log.info(to_dict(message))

    except:
        log.error(traceback.format_exc())
    return HttpResponse(response, content_type="application/xml")


def tool_add():
    for u in UserProfile.objects.all():
        if u.vehicle and not u.vehicles:
            u.vehicles = '["%s"]' % u.vehicle.imei
            u.save()
            print(u.vehicle.imei)