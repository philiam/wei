# coding: utf-8
from __future__ import unicode_literals

__author__ = 'zhangtao'

from zbase.tools import *
import urllib
import json


def get_robot_reply(text, openid):
    log.info('发起机器人请求')
    url = 'http://www.tuling123.com/openapi/api'
    data = {
        'key': '356eb9ff48d0bbdc1de37d3a6a76e4aa',
        'info': text,
        'userid': openid,
    }

    d = urllib.urlencode(data)
    url += '?' + d
    log.debug('请求地址：%s' % url)
    bs = get_http_string(url)
    jc = json.loads(bs)
    log.debug('返回内容： %s' % jc)

    t = ''
    if 'text' in jc:
        jc['text'] = jc['text'].replace('图灵机器人', '小康康')
    # 文字类
    if jc['code'] == 100000:
        t = jc['text']
    elif jc['code'] == 200000:
        t = '%s<a href="%s">【详情】</a>' % (jc['text'], jc['url'])
    else:
        t = '咱们聊点其他的吧。。'
    return t