# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='WechatMenu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('num', models.IntegerField(null=True, verbose_name='No.', blank=True)),
                ('name', models.CharField(max_length=100, null=True, verbose_name='name', blank=True)),
                ('type', models.CharField(max_length=20, null=True, verbose_name='type', blank=True)),
                ('key', models.CharField(max_length=1000, null=True, verbose_name='key', blank=True)),
                ('top_menu', models.BooleanField(default=False, verbose_name='top menu')),
                ('valid', models.BooleanField(default=True, verbose_name='valid')),
                ('parent', models.ForeignKey(to='core.WechatMenu', null=True)),
                ('wechat', models.ForeignKey(to='core.WechatAccount', null=True)),
            ],
            options={
                'ordering': ['top_menu', 'num'],
                'db_table': 'wx_menu',
                'verbose_name': 'Wechat Menu',
                'verbose_name_plural': 'Wechat Menu',
            },
        ),
    ]
