# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AccessToken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('access_token', models.CharField(max_length=200, null=True, verbose_name='AccessToken', blank=True)),
                ('expires_in', models.CharField(max_length=50, null=True, verbose_name='Token', blank=True)),
                ('stamp', models.IntegerField(default=0, null=True, verbose_name='stamp', blank=True)),
                ('appid', models.CharField(max_length=50, null=True, verbose_name='Appid', blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Datetime')),
            ],
            options={
                'db_table': 'wx_access_token',
                'verbose_name': 'Access Token',
                'verbose_name_plural': 'Access Token',
            },
        ),
        migrations.CreateModel(
            name='WechatAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, null=True, verbose_name='name', blank=True)),
                ('token', models.CharField(max_length=50, null=True, verbose_name='token', blank=True)),
                ('appid', models.CharField(max_length=50, null=True, verbose_name='appid', blank=True)),
                ('appsecret', models.CharField(max_length=50, null=True, verbose_name='appsecret', blank=True)),
                ('init_id', models.CharField(max_length=50, null=True, verbose_name='init_id', blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Datetime')),
            ],
            options={
                'db_table': 'wx_account',
                'verbose_name': 'Account',
                'verbose_name_plural': 'Account',
            },
        ),
    ]
