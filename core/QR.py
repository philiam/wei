# coding: utf-8
from __future__ import unicode_literals

__author__ = 'zhangtao'
from core.models import AccessToken, WechatAccount
import time
import urllib2
import json
from zbase.tools import *

"""
    处理 access token 及 生成二维码
"""


# 获取access_token
def get_access_token(appid=''):
    if appid:
        ats = AccessToken.objects.filter(appid=appid)
    else:
        ats = AccessToken.objects.all()
    if len(ats) > 0:
        at = ats[0]
        s_t = int(time.time())
        if s_t - at.stamp < 5000:
            log.info('未过期直接返回')
            return at.access_token
        log.info('已过期 重新获取')

    if appid:
        wa = WechatAccount.objects.get(appid=appid)
    else:
        wa = WechatAccount.objects.all()[0]
    url = 'https://api.weixin.qq.com/cgi-bin/token?' \
          'grant_type=client_credential' \
          '&appid=%s&secret=%s' % (wa.appid, wa.appsecret)

    f = urllib2.urlopen(url)
    r = f.read()
    log.debug('接口返回结果：%s' % r)
    jr = json.loads(r)
    ats = AccessToken.objects.get_or_create(appid=wa.appid)
    at = ats[0]
    s_now = int(time.time())
    at.stamp = s_now
    at.access_token = jr['access_token']
    at.save()
    return jr['access_token']


# 生成带参数的微信二维码
def getQRCode(appid, str, ):
    log.info('生成二维码：appid=%s ;  arg=%s' % (appid, str))
    token = get_access_token(appid)
    url = ''
    post_data = {
        # "expire_seconds": 1800,
        # "action_name": "QR_SCENE",
        "action_name": "QR_LIMIT_STR_SCENE",
        "action_info": {"scene": {"scene_str": str}}
    }

    url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=%s" % token
    log.info("发起微信二维码生成请求")
    log.debug(post_data)

    f = urllib2.urlopen(url, json.dumps(post_data, ensure_ascii=False))
    r = f.read()
    log.info("收到返回数据")
    log.debug(r)
    jr = json.loads(r)
    pic_url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=%s" % jr['ticket']
    log.info('二维码地址：%s' % pic_url)
    return pic_url

