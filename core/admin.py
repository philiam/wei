# coding: utf-8
from django.contrib import admin
from core.models import *
# Register your models here.
class WechatAccountAdmin(admin.ModelAdmin):
    list_display = ('name', 'token', 'appid', 'appsecret', 'date_created')
    search_fields = ('name', )
    # list_filter = ('imei',)

    # 编辑项
    # fieldsets = (
    #     (None, {'fields': ('imei', 'image')}),
    # )
admin.site.register(WechatAccount, WechatAccountAdmin)


class AccessTokenAdmin(admin.ModelAdmin):
    list_display = ('appid', 'access_token', 'expires_in', 'stamp', 'op', 'date_created')
    search_fields = ('appid', )
    # list_filter = ('imei',)
    # 编辑项
    # fieldsets = (
    #     (None, {'fields': ('imei', 'image')}),
    # )

    def op(self, obj):
        return '<a href="/api/refresh/access_token?appid=%s" class="btn btn-warn"> access_token 刷新 </a>'% obj.appid
    op.allow_tags = True
    op.short_description = '操作'
admin.site.register(AccessToken, AccessTokenAdmin)


class WechatMenuAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'key', 'num', 'valid', 'top_menu', 'wechat')
    search_fields = ('name', 'key')
    # list_filter = ('imei',)
    # 编辑项
    # fieldsets = (
    # (None, {'fields': ('imei', 'image')}),
    # )


admin.site.register(WechatMenu, WechatMenuAdmin)