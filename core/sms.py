# coding:utf-8

import requests
import hashlib
import time


# 改用新短信接口
def quectel(num='1064868406007', content=''):
    url = 'http://139.196.41.136:8098/openapi/router'
    app_secret = 'acdef0dfq'
    appkey = '5d428b3263'
    data = {
        'appkey': appkey,
        't': int(time.time()),
        'method': 'fc.function.sms.send',
        'msisdns': num,
        'content': content
    }

    sorted_data = sorted(data.items(), key=lambda d: d[0])
    # print(sorted_data)

    s = app_secret
    for k in sorted_data:
        s += '{}{}'.format(k[0], k[1])

    s += app_secret
    sign = hashlib.sha1(s).hexdigest()

    # print(s)
    data['sign'] = sign
    # import json
    # print (json.dumps(data,indent=4))
    rs = requests.post(url, data=data)
    # {"successList":[{"msisdn":1064868406007,"sms_id":"1235799"}],"errorList":[],"resultCode":0,"errorMessage":""}
    if len(rs.json()['successList']) < 1:
        return False
    else:
        return True
