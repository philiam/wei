# -+- coding: utf-8 -+-
#
# Created by philiam on 2015/11/16.
#
from __future__ import unicode_literals

__author__ = 'philiam'
from zbase.tools import *
import paho.mqtt.client as mqtt
import json
import traceback
from wei.settings import *
from car.views import pub_sms


# 单个文件测试 不导入
if __name__ != '__main__':
    from car.templatemsg import batvolt_low_alarm, leave_alarm, default_alarm


class MqttMsgHandler(object):
    """
    mqtt 消息处理中心
    """
    ip = ''
    port = 1883

    def __init__(self, ip='localhost', port=1883):
        self.ip = ip
        self.port = port
        self.started = False
        self.client = mqtt.Client(protocol=mqtt.MQTTv31)
        self.client._client_id = gl['daemon_id']
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.on_disconnect = self.on_disconnect
        self.client.username_pw_set('covixon', 'covixon123')

    def __del__(self):
        self.client.loop_stop()
        log.debug('mqtt client stop')

    @staticmethod
    def on_publish(client, userdata, mid):
        log.debug(".on_publish userdata=%s,mid=%s." % (userdata, mid))

    @staticmethod
    def on_connect(client, userdata, flags, rc):
        # logging.debug()
        log.debug(client._client_id)
        log.debug("Connected with result code " + str(rc))

    @staticmethod
    def on_disconnect(client, userdata, rc):
        log.debug("on_disconnect userdata=%s,rc=%s." % (userdata, rc))

    @staticmethod
    def on_message(client, userdata, msg):
        log.debug("收到mqtt消息：topic=%s, payload=%s" % (msg.topic , str(msg.payload)))
        try:
            grid = json.loads(msg.payload)
            if 'alarm' in grid:
                # log.debug('收到报警：%s' % msg.payload)
                # {
                # "alarm":"batvolt_low_200",
                # "alarm_batvoltlow_ts":"2016-11-28 15:06:31",
                # "batvolt":10899
                # }
                if grid['alarm'].startswith('batvolt_low'):
                    # 低电压报警
                    batvolt_low_alarm(grid)
                elif grid['alarm'] == 'leave':
                    # 锁车报警
                    leave_alarm(grid)
                else:
                    # 其他未知报警
                    default_alarm(grid)
                    # pass
        except:
            log.error(traceback.format_exc())

    def publish(self, topic, payload, qos=0):
        if topic:
            self.check_started()

            # 解锁和升级时候发送 短信唤醒
            # UNLock,Lock, Upd
            if payload.__contains__('Lock') or payload.__contains__('"Act":"Upd"'):
                # 发短信唤醒
                pub_sms(topic)
            log.debug('发送mqtt消息 topic=%s, payload=%s' % (topic, payload))
            self.client.publish(topic, payload, qos)

    def check_started(self):
        if not self.started:
            self.client.connect(self.ip, self.port, 1800, )
            self.client.loop_start()
            self.started = True
            log.debug('mqtt client start')
            return 1
        return 2


mq = MqttMsgHandler(gl['mqtt']['host'])


if __name__ == '__main__':
    ah = MqttMsgHandler('211.154.146.61')
    import time
    print ah.client._state
    time.sleep(1)
    ah.publish('r', '{"act":"test2"}', 1)
    time.sleep(1)
    ah.client.disconnect()
