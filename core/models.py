# coding: utf-8
from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _
from zbase.tools import log
import traceback

MENU_TYPE = (
    ('click', _('按钮')),
    ('view', _('链接')),
)

# Create your models here.
class WechatAccount(models.Model):
    name = models.CharField(_('name'), max_length=50, null=True, blank=True)
    token = models.CharField(_('token'), max_length=50, null=True, blank=True)
    appid = models.CharField(_('appid'), max_length=50, null=True, blank=True)
    appsecret = models.CharField(_('appsecret'), max_length=50, null=True, blank=True)
    init_id = models.CharField(_('init_id'), max_length=50, null=True, blank=True)
    date_created = models.DateTimeField(_('Datetime'), auto_now_add=True)

    class Meta:
        verbose_name = _('Account')
        verbose_name_plural = _('Account')
        db_table = 'wx_account'
        # ordering = ['-id']

    def __unicode__(self):
        return self.name


class AccessToken(models.Model):
    access_token = models.CharField(_('AccessToken'), max_length=200, null=True, blank=True)
    expires_in = models.CharField(_('Token'), max_length=50, null=True, blank=True)
    stamp = models.IntegerField(_('stamp'), default=0, null=True, blank=True)
    appid = models.CharField(_('Appid'), max_length=50, null=True, blank=True)
    date_created = models.DateTimeField(_('Datetime'), auto_now_add=True)

    class Meta:
        verbose_name = _('Access Token')
        verbose_name_plural = _('Access Token')
        db_table = 'wx_access_token'
        # ordering = ['-id']

    def __unicode__(self):
        return self.appid


class WechatMenu(models.Model):
    num = models.IntegerField(_('No.'), default=0, null=True, blank=True)
    name = models.CharField(_('name'), max_length=100, null=True, blank=True)
    type = models.CharField(_('type'), null=True, choices=MENU_TYPE, max_length=20, blank=True)
    key = models.CharField(_('key'), max_length=1000, null=True, blank=True)
    top_menu = models.BooleanField(_('top menu'), default=False, blank=True)
    valid = models.BooleanField(_('valid'), default=True, blank=True)
    parent = models.ForeignKey('WechatMenu', null=True, blank=True)
    wechat = models.ForeignKey(WechatAccount, null=True)

    class Meta:
        verbose_name = _('Wechat Menu')
        verbose_name_plural = _('Wechat Menu')
        db_table = 'wx_menu'
        ordering = ['parent__pk', 'id', 'top_menu', 'num']

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        log.debug('WechatMenu model save %s' % self.name)
        try:
            if not self.parent:
                super(self.__class__, self).save(*args, **kwargs)
                self.parent = self
        except:
            log.error(traceback.format_exc())
        super(self.__class__, self).save(*args, **kwargs)