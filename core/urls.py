"""wei URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from zbase.tools import *
from core.views import *

use_unicode()

# urlpatterns = [
#     url(r'^$', 'core.views.weixin_main'),
#     url(r'^main', 'core.views.weixin_main'),
#     url(r'^mqtt', 'core.views.mqtt_test'),
#     url(r'^qrcode/(?P<appid>\w+)?/(?P<str>\w+)/$', 'core.views.get_qrcode'),
#     url(r'^oauth2/(?P<menu_id>\w+)/$', 'core.views.oauth2')
# ]

urlpatterns = [
    url(r'^$', weixin_main),
    url(r'^main', weixin_main),
    url(r'^mqtt', mqtt_test),
    url(r'^refresh/access_token', refresh_access_token),
    url(r'^qrcode/(?P<appid>\w+)?/(?P<str>\w+)/$', get_qrcode),
    url(r'^oauth2/(?P<menu_id>\w+)/$', oauth2),

]
